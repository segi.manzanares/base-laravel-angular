import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { ImageCropperComponent } from './components/image-cropper/image-cropper.component';
import { SortableDirective } from './directives/sortable.directive';

@NgModule({
    declarations: [
        ImageCropperComponent,
        SortableDirective,
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
    ],
    exports: [
        ImageCropperComponent,
        SortableDirective,
    ]
})
export class ComponentsModule {
    static forRoot(): ModuleWithProviders<ComponentsModule> {
        return {
            ngModule: ComponentsModule
        }
    }
}
