import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ToastrModule } from 'ngx-toastr';
import { NgxLoadingModule } from 'ngx-loading';
import { NgbModule, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from './reducers';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { EffectsModule } from '@ngrx/effects';
import { HomeComponent } from './modules/home/home-page/home.component';
import { ComponentsModule } from './components.module';
import { StoreRouterConnectingModule, RouterState } from '@ngrx/router-store';
import { HomeEffects } from './modules/home/home.effects';
import { CatalogEffects } from './modules/catalogues/catalog.effects';

// Modules
import { AuthModule } from './modules/auth/auth.module';
import { UserModule } from './modules/users/user.module';

// Layout
import { MainComponent } from './layout/main/main.component';
import { DefaultComponent } from './layout/default/default.component';
import { HeaderComponent } from './layout/header/header.component';
import { FooterComponent } from './layout/footer/footer.component';
import { MenuSidebarComponent } from './layout/menu-sidebar/menu-sidebar.component';
import { MessagesDropdownMenuComponent } from './layout/header/messages-dropdown-menu/messages-dropdown-menu.component';
import { NotificationsDropdownMenuComponent } from './layout/header/notifications-dropdown-menu/notifications-dropdown-menu.component';

import { NgbDateCustomParserFormatter } from './extensions/ngbdate.parserformatter';
import './extensions/global';

export function HttpLoaderFactory(httpClient: HttpClient) {
    return new TranslateHttpLoader(httpClient);
}


@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        MainComponent,
        DefaultComponent,
        HeaderComponent,
        FooterComponent,
        MenuSidebarComponent,
        MessagesDropdownMenuComponent,
        NotificationsDropdownMenuComponent,
    ],
    imports: [
        FormsModule,
        ReactiveFormsModule,
        BrowserModule,
        HttpClientModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        NgbModule,
        NgxLoadingModule.forRoot({}),
        ToastrModule.forRoot(),
        ComponentsModule.forRoot(),
        AuthModule.forRoot(),
        UserModule.forRoot(),
        StoreModule.forRoot(reducers, {
            metaReducers,
            runtimeChecks: {
                strictStateImmutability: true,
                strictActionImmutability: true,
                strictActionSerializability: true,
                strictStateSerializability: true
            }
        }),
        StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),
        EffectsModule.forRoot([HomeEffects, CatalogEffects]),
        StoreRouterConnectingModule.forRoot({
            stateKey: 'router',
            routerState: RouterState.Minimal
        }),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
    ],
    providers: [
        { provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter },
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
