import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { User, IUser } from '../../models/user';
import { IApiError } from '../../models/apierror';
import { TableOptions } from './../../models/tableoptions';
import { ApiListResult, ApiCreateResult, ApiUpdateResult } from './../../models/apiresponse';

export enum UserActionTypes {
    LoadUsers = '[User] Load Users',
    LoadUsersSuccess = '[User] Load Users Success',
    AddUser = '[User] Add User',
    AddUserSuccess = '[User] Add User Success',
    AddUserFails = '[User] Add User Fails',
    EditUser = '[User] Edit User',
    UpdateUser = '[User] Update User',
    UpdateUserSuccess = '[User] Update User Success',
    UpdateUserFails = '[User] Update User Fails',
    DeleteUser = '[User] Delete User',
    DeleteUserSuccess = '[User] Delete User Success',
    DeleteUserFails = '[User] Delete User Fails',
    ToggleUser = '[User] Toggle User',
    ToggleUserSuccess = '[User] Toggle User Success',
}

export class LoadUsers implements Action {
    readonly type = UserActionTypes.LoadUsers;

    constructor(public payload: TableOptions) { }
}

export class LoadUsersSuccess implements Action {
    readonly type = UserActionTypes.LoadUsersSuccess;

    constructor(public payload: ApiListResult<User>) { }
}

export class AddUser implements Action {
    readonly type = UserActionTypes.AddUser;

    constructor(public payload: IUser) { }
}

export class AddUserSuccess implements Action {
    readonly type = UserActionTypes.AddUserSuccess;

    constructor(public payload: ApiCreateResult<IUser>) { }
}

export class AddUserFails implements Action {
    readonly type = UserActionTypes.AddUserFails;

    constructor(public payload: IApiError) { }
}

export class EditUser implements Action {
    readonly type = UserActionTypes.EditUser;

    constructor(public payload: IUser) { }
}

export class UpdateUser implements Action {
    readonly type = UserActionTypes.UpdateUser;

    constructor(public payload: IUser) { }
}

export class UpdateUserSuccess implements Action {
    readonly type = UserActionTypes.UpdateUserSuccess;

    constructor(public payload: ApiUpdateResult<Update<IUser>>) { }
}

export class UpdateUserFails implements Action {
    readonly type = UserActionTypes.UpdateUserFails;

    constructor(public payload: IApiError) { }
}

export class DeleteUser implements Action {
    readonly type = UserActionTypes.DeleteUser;

    constructor(public payload: { id: number }) { }
}

export class DeleteUserSuccess implements Action {
    readonly type = UserActionTypes.DeleteUserSuccess;

    constructor(public payload: { id: string, message: string }) { }
}

export class DeleteUserFails implements Action {
    readonly type = UserActionTypes.DeleteUserFails;

    constructor(public payload: IApiError) { }
}

export class ToggleUser implements Action {
    readonly type = UserActionTypes.ToggleUser;

    constructor(public payload: { id: number }) { }
}

export class ToggleUserSuccess implements Action {
    readonly type = UserActionTypes.ToggleUserSuccess;

    constructor(public payload: { user: Update<User>, message: string }) { }
}

export type UserActions =
    LoadUsers
    | LoadUsersSuccess
    | AddUser
    | AddUserSuccess
    | AddUserFails
    | EditUser
    | UpdateUser
    | UpdateUserSuccess
    | UpdateUserFails
    | DeleteUser
    | DeleteUserSuccess
    | DeleteUserFails
    | ToggleUser
    | ToggleUserSuccess;
