import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { User, IUser } from './../../models/user';
import { TableOptions } from './../../models/tableoptions';
import { ApiService } from './../../services/api.service';
import { ApiListResult, ApiCreateResult, ApiUpdateResult } from './../../models/apiresponse';
import { map } from 'rxjs/operators';
import { Role } from "../../models/role";

@Injectable()
export class UserService {

    constructor(
        private apiService: ApiService
    ) { }

    public getUser(id: number): Observable<User> {
        return this.apiService.request('get', `users/${id}`, {}).pipe(
            map(resp => User.fromJson(resp))
        );
    }

    public getUsers(options: TableOptions): Observable<ApiListResult<User>> {
        let filters = {
            ...options.filters,
            page: options.pagination.page,
            take: options.pagination.take,
            sort: options.sort,
        }
        return this.apiService.request('get', 'users', filters);
    }

    public createUser(data: IUser): Observable<ApiCreateResult<IUser>> {
        let user = User.fromJson(data);
        user.role = Role.empty();
        user.role.id = data.roleId;
        return this.apiService.request('post', 'users', user.toApiRequest());
    }

    public updateUser(data: IUser): Observable<ApiUpdateResult<IUser>> {
        let user = User.fromJson(data);
        user.role = Role.empty();
        user.role.id = data.roleId;
        return this.apiService.request('put', `users/${data.id}`, user.toApiRequest());
    }

    public deleteUser(id: number): Observable<string> {
        return this.apiService.request('delete', `users/${id}`, {})
            .pipe(
                map(resp => resp.message)
            );
    }

    public toggleUserStatus(id: number): Observable<{message: string, isActive: boolean}> {
        return this.apiService.request('put', `users/${id}/status`, {});
    }

    public invite(id: number): Observable<{message: string}> {
        return this.apiService.request('post', `users/${id}/invite`, {});
    }
}