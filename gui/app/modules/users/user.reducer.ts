import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { IUser } from '../../models/user';
import { UserActions, UserActionTypes } from './user.actions';
import { IApiError } from '../../models/apierror';
import { TableOptions } from './../../models/tableoptions';

export const usersFeatureKey = 'users';

export interface UserState extends EntityState<IUser> {
    total: number,
    loading: boolean,
    options: TableOptions,
    error: IApiError,
    successMessage: string,
    editingUser: IUser,
}

export const adapter: EntityAdapter<IUser> = createEntityAdapter<IUser>();

export const initialState: UserState = adapter.getInitialState({
    total: 0,
    loading: false,
    options: null,
    error: null,
    successMessage: null,
    editingUser: null
});

export function reducer(
    state = initialState,
    action: UserActions
): UserState {
    switch (action.type) {
        case UserActionTypes.AddUser: {
            return { ...state, loading: true, error: null };
        }
        case UserActionTypes.AddUserSuccess: {
            return adapter.addOne(
                action.payload.data,
                { ...state, loading: false, successMessage: action.payload.message, total: state.total + 1 }
            );
        }
        case UserActionTypes.EditUser: {
            return { ...state, loading: false, error: null, editingUser: action.payload };
        }
        case UserActionTypes.UpdateUser: {
            return { ...state, loading: true, error: null, editingUser: null };
        }
        case UserActionTypes.UpdateUserSuccess: {
            return adapter.updateOne(
                action.payload.data,
                { ...state, loading: false, successMessage: action.payload.message }
            );
        }
        case UserActionTypes.DeleteUser: {
            return { ...state, loading: true, error: null };
        }
        case UserActionTypes.DeleteUserSuccess: {
            return adapter.removeOne(
                action.payload.id,
                { ...state, loading: false, successMessage: action.payload.message, total: state.total - 1 }
            );
        }
        case UserActionTypes.AddUserFails:
        case UserActionTypes.UpdateUserFails:
        case UserActionTypes.DeleteUserFails: {
            return { ...state, loading: false, error: action.payload };
        }
        case UserActionTypes.LoadUsers: {
            return adapter.removeAll({
                ...state,
                loading: true,
                options: action.payload
            });
        }
        case UserActionTypes.LoadUsersSuccess: {
            return adapter.setAll(action.payload.data, { ...state, loading: false, total: action.payload.total });
        }
        case UserActionTypes.ToggleUser: {
            return { ...state, loading: true, error: null };
        }
        case UserActionTypes.ToggleUserSuccess: {
            return adapter.updateOne(
                action.payload.user,
                { ...state, loading: false, successMessage: action.payload.message }
            );
        }
        default: {
            return state;
        }
    }
}

export const {
    selectIds,
    selectEntities,
    selectAll,
    selectTotal,
} = adapter.getSelectors();
