import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromUser from './user.reducer';
import { User } from '../../models/user';

export const selectFeature = createFeatureSelector<fromUser.UserState>(fromUser.usersFeatureKey);

export const selectUserState = createSelector(
    selectFeature,
    (state: fromUser.UserState) => state,
)
export const getUsersRaw = createSelector(
    selectUserState,
    fromUser.selectAll
);

export const getUsers = createSelector(
    getUsersRaw,
    users => users.map(u => User.fromJson(u))
);

export const getTotal = createSelector(
    selectUserState,
    (state: fromUser.UserState) => state.total
)

export const getLoading = createSelector(
    selectUserState,
    (state: fromUser.UserState) => state.loading
)

export const getError = createSelector(
    selectUserState,
    (state: fromUser.UserState) => state.error
)

export const getEditingUser = createSelector(
    selectUserState,
    (state: fromUser.UserState) => state.editingUser
)