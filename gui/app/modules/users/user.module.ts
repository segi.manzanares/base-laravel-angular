import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserFormComponent } from './user-form/user-form.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { NgxLoadingModule } from 'ngx-loading';
import { ComponentsModule } from './../../components.module';
import { UserService } from './user.service';
import { UsersListComponent } from './users-list/users-list.component';
import { StoreModule } from '@ngrx/store';
import { reducer } from './user.reducer';
import { UserEffects } from './user.effects';
import { RouterModule, Routes } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { usersFeatureKey } from './user.reducer';

const routes: Routes = [
    {
        path: '', component: UsersListComponent, children: [
            { path: ':id/edit', component: UsersListComponent, data: { title: "Edit user" } },
        ],
        data: { title: "Users" }
    },
];

@NgModule({
    declarations: [
        UserFormComponent,
        UsersListComponent
    ],
    imports: [
        CommonModule,
        ComponentsModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        TranslateModule,
        RouterModule.forChild(routes),
        StoreModule.forFeature(usersFeatureKey, reducer),
        EffectsModule.forFeature([UserEffects]),
        NgxLoadingModule.forRoot({}),
    ],
    exports: [
        UserFormComponent
    ]
})
export class UserModule {
    static forRoot(): ModuleWithProviders<UserModule> {
        return {
            ngModule: UserModule,
            providers: [
                UserService,
            ]
        }
    }
}
