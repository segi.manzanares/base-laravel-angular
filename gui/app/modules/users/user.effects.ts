import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Router } from '@angular/router';
import { UserService } from './user.service';
import { AuthService } from './../auth/auth.service';
import { map, exhaustMap, catchError, switchMap, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import { ApiError } from '../../models/apierror';
import { Update } from '@ngrx/entity';
import { IUser, User } from '../../models/user';
import {
    UserActionTypes,
    LoadUsers,
    LoadUsersSuccess,
    AddUser,
    AddUserSuccess,
    AddUserFails,
    EditUser,
    UpdateUser,
    UpdateUserSuccess,
    UpdateUserFails,
    DeleteUser,
    DeleteUserSuccess,
    DeleteUserFails,
    ToggleUser,
    ToggleUserSuccess,
} from './user.actions';
import { UpdateProfileSuccess } from '../auth/auth.actions';

@Injectable()
export class UserEffects {

    @Effect()
    loadUsers$ = this.actions$.pipe(
        ofType(UserActionTypes.LoadUsers),
        map((action: LoadUsers) => action.payload),
        exhaustMap((payload) =>
            this.userService
                .getUsers(payload)
                .pipe(
                    map(resp => new LoadUsersSuccess(resp))
                )
        )
    );

    @Effect()
    addUser$ = this.actions$.pipe(
        ofType(UserActionTypes.AddUser),
        map((action: AddUser) => action.payload),
        exhaustMap((payload: IUser) =>
            this.userService
                .createUser(payload)
                .pipe(
                    map(resp => new AddUserSuccess(resp)),
                    catchError((error: ApiError) => {
                        return of(new AddUserFails(Object.assign({}, error)));
                    })
                )
        )
    );

    @Effect({ dispatch: false })
    editUser$ = this.actions$.pipe(
        ofType(UserActionTypes.EditUser),
        map((action: EditUser) => action.payload),
        tap((user) => {
            this.router.navigate([`/users/${user.id}/edit`]);
        })
    );

    @Effect()
    updateUser$ = this.actions$.pipe(
        ofType(UserActionTypes.UpdateUser),
        map((action: UpdateUser) => action.payload),
        exhaustMap((payload: IUser) =>
            this.userService
                .updateUser(payload)
                .pipe(
                    switchMap(resp => {
                        const updatedUser: Update<IUser> = {
                            id: payload.id,
                            changes: resp.data
                        };
                        // Checar si se actualizó el usuario logueado
                        let authUser = this.authService.getAuthenticatedUser();
                        let actions = [];
                        if (authUser.id == updatedUser.id) {
                            actions.push(new UpdateProfileSuccess({
                                message: "Perfil actualizado satisfactoriamente",
                                data: {...resp.data}
                            }));
                        }
                        actions.push(new UpdateUserSuccess({ data: updatedUser, message: resp.message }));
                        return actions;
                    }),
                    catchError((error: ApiError) => {
                        return of(new UpdateUserFails(Object.assign({}, error)));
                    })
                )
        )
    );

    @Effect()
    deleteUser$ = this.actions$.pipe(
        ofType(UserActionTypes.DeleteUser),
        map((action: DeleteUser) => action.payload.id),
        exhaustMap((payload: number) =>
            this.userService
                .deleteUser(payload)
                .pipe(
                    map(resp => {
                        return new DeleteUserSuccess({ id: payload + "", message: resp });
                    }),
                    catchError((error: ApiError) => {
                        return of(new DeleteUserFails(Object.assign({}, error)));
                    })
                )
        )
    );

    @Effect()
    toggleUser$ = this.actions$.pipe(
        ofType(UserActionTypes.ToggleUser),
        map((action: ToggleUser) => action.payload.id),
        exhaustMap((payload: number) =>
            this.userService
                .toggleUserStatus(payload)
                .pipe(
                    map(resp => {
                        const updatedUser: Update<User> = {
                            id: payload,
                            changes: {isActive: resp.isActive}
                        };
                        return new ToggleUserSuccess({ user: updatedUser, message: resp.message });
                    })
                )
        )
    );

    constructor(
        private router: Router,
        private actions$: Actions,
        private userService: UserService,
        private authService: AuthService,
    ) { }
}
