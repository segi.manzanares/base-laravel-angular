import { UserService } from './../user.service';
import { Component, OnInit, ViewChild, ViewChildren, QueryList, Inject } from '@angular/core';
import { NgbModal, NgbModalConfig, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { SortableDirective, SortEvent } from '../../../directives/sortable.directive';
import { WindowService } from '../../../services/window.service';
import { ToastrService } from 'ngx-toastr';
import { Store, select, ActionsSubject } from '@ngrx/store';
import * as fromUser from '../user.actions';
import { AppState } from '../../../reducers';
import { Observable, Subscription } from 'rxjs';
import { User } from '../../../models/user';
import { getUsers, getLoading, getError, getTotal, getEditingUser } from '../user.selectors';
import Utils from '../../../utils';
import { UserFormComponent } from '../user-form/user-form.component';
import { defaultNgxLoadingConfig } from '../../../config/index';
import { filter } from 'rxjs/operators';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { TableOptions } from './../../../models/tableoptions';

@Component({
    selector: 'app-users-list',
    templateUrl: './users-list.component.html',
    styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {
    @ViewChild('userModal', {static: false}) userModal: NgbModal;
    @ViewChildren(SortableDirective) headers: QueryList<SortableDirective>;
    public userForm: UserFormComponent;
    public users$: Observable<User[]>;
    public loading$: Observable<boolean>;
    public total$: Observable<number>;
    public errorSubscription$: Subscription;
    public routerSubscription$: Subscription;
    public activatedRouteSubscription$: Subscription;
    public userSubscription$: Subscription;
    public modalReference: NgbModalRef;
    public modal: {[k: string]: any} = {
        title: 'Create user',
        mode: 'create'
    };
    public loaderConfig = defaultNgxLoadingConfig;
    public options: TableOptions = {
        pagination: {
            page: 1,
            take: 20
        },
        sort: 'name|asc',
        filters: {
            name: '',
            email: '',
        }
    }
    public selectedUser: User;
    public modalOpen: boolean = false;

    constructor(
        private store: Store<AppState>,
        config: NgbModalConfig,
        public userService: UserService,
        private modalService: NgbModal,
        public windowService: WindowService,
        private toastr: ToastrService,
        public translate: TranslateService,
        private actionsSubject$: ActionsSubject,
        private activatedRoute: ActivatedRoute,
        private router: Router
    ) {
        config.backdrop = 'static';
        config.keyboard = false;
    }

    ngOnInit(): void {
        this.getUsers();
        this.users$ = this.store.pipe(select(getUsers));
        this.loading$ = this.store.pipe(select(getLoading));
        this.total$ = this.store.pipe(select(getTotal));
        this.errorSubscription$ = this.store.pipe(select(getError)).subscribe({
            next: error => {
                if (error) {
                    if (this.userForm) {
                        this.userForm.errors = error.errors;
                    }
                    this.toastr.error(error.message);
                    if (error.statusCode === 401 && this.modalReference) {
                        this.modalReference.close();
                    }
                }
            }
        });
        this.actionsSubject$.pipe(
            filter((action: any) => action.type === fromUser.UserActionTypes.AddUserSuccess
                || action.type === fromUser.UserActionTypes.UpdateUserSuccess
                || action.type === fromUser.UserActionTypes.DeleteUserSuccess
                || action.type === fromUser.UserActionTypes.ToggleUserSuccess)
        ).subscribe((action: any) => {
            this.toastr.success(action.payload.message);
            if (this.modalReference) {
                this.modalReference.close();
            }
        });
        // Si se ingresa directo a la url /users/:id/edit
        if (this.activatedRoute.firstChild != null
                && this.activatedRoute.firstChild.routeConfig.path === ':id/edit') {
            this.activatedRouteSubscription$ =
                this.activatedRoute.firstChild.paramMap.subscribe(params => {
                    this.showUser(parseInt(params.get('id')));
                });
        }
        // Si estando dentro del listado de usuarios se presiona el botón para
        // editar usuarios
        this.routerSubscription$ = this.router.events.subscribe((event) => {
            if (event instanceof NavigationEnd) {
                if (/\/users\/[0-9]+\/edit$/.test(event.url)) {
                    this.getEditingUserFromStore();
                }
                else if (event.url === '/users') {
                    if (this.userSubscription$) {
                        this.userSubscription$.unsubscribe();
                    }
                }
            }
        });
    }

    ngOnDestroy() {
        this.routerSubscription$.unsubscribe();
        if (this.userSubscription$) {
            this.userSubscription$.unsubscribe();
        }
        if (this.activatedRouteSubscription$) {
            this.activatedRouteSubscription$.unsubscribe();
        }
        this.errorSubscription$.unsubscribe();
    }

    public getEditingUserFromStore() {
        this.userSubscription$ = this.store.pipe(select(getEditingUser)).subscribe({
            next: user => {
                if (user !== null) {
                    this.editUser(User.fromJson(user));
                }
            }
        });
    }

    public showUser(id: number) {
        this.userSubscription$ = this.userService.getUser(id).subscribe({
            next: user => this.editUser(user)
        });
    }

    get page() {
        return this.options.pagination.page;
    }
    get take() {
        return this.options.pagination.take;
    }

    set page(page: number) {
        this.options.pagination.page = page;
        this.getUsers();
    }
    set take(take: number) {
        this.options.pagination.take = take;
        this.getUsers();
    }

    public open(content: NgbModal) {
        this.modalOpen = true;
        this.modalReference = this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'});
        this.modalReference.result.then((result) => {
            this.modalOpen = false;
            this.router.navigate(['/users'], { replaceUrl: true });
        }, (reason) => {
            this.modalOpen = false;
            this.router.navigate(['/users'], { replaceUrl: true });
        });
    }

    public onSort({column, direction}: SortEvent) {
        // Reset other headers
        this.headers.forEach(header => {
            if (header.sortable !== column) {
                header.direction = '';
            }
        });
        this.options.sort = column + '|' + direction;
        this.getUsers();
    }

    private getUsers(): void {
        this.store.dispatch(new fromUser.LoadUsers({
            ...this.options,
            pagination: {...this.options.pagination},
            filters: {...this.options.filters, type: 'admin'}
        }));
    }

    public createUser() {
        this.selectedUser = null;
        this.modal.title = 'Create user';
        this.modal.mode = 'create';
        this.open(this.userModal);
    }

    public goEditUser(user: User) {
        this.store.dispatch(new fromUser.EditUser({...user, role: {...user.role}}))
    }

    public editUser(user: User) {
        this.selectedUser = user;
        this.modal.title = 'Edit user';
        this.modal.mode = 'edit';
        this.open(this.userModal);
    }

    public filter() {
        this.getUsers();
    }

    public onSubmit(userForm: UserFormComponent): void {
        this.userForm = userForm;
        this.userForm.submit();
    }

    public deleteUser(user: User) {
        const self = this;
        let title = "Delete user";
        let msg = "Are you sure to delete user {{name}}";
        this.translate.get(["Accept", "Cancel", title, msg], {name: user.fullName}).subscribe((trans) => {
            Utils.confirm({
                title: trans[title],
                msg: trans[msg],
                confirmButtonText: trans["Accept"],
                cancelButtonText: trans["Cancel"],
                icon: 'question',
                callback: function() {
                    self.store.dispatch(new fromUser.DeleteUser({id : user.id}));
                }
            });
        });
    }

    public toggleUserStatus(user: User) {
        const self = this;
        let title = (user.isActive ? 'Disable' : 'Enable') + " user";
        let msg = "Are you sure to " + (user.isActive ? 'disable' : 'enable') + " user {{name}}";
        this.translate.get(["Accept", "Cancel", title, msg], {name: user.fullName}).subscribe((trans) => {
            Utils.confirm({
                title: trans[title],
                msg: trans[msg],
                confirmButtonText: trans["Accept"],
                cancelButtonText: trans["Cancel"],
                type: 'question',
                callback: function(value: string) {
                    self.store.dispatch(new fromUser.ToggleUser({id : user.id}));
                }
            });
        });
    }
}
