import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ImageData } from '../../../components/image-cropper/image-cropper.component';
import { Observable } from 'rxjs';
import { select, Store } from '@ngrx/store';
import { MustMatch } from '../../../helpers';
import { User } from '../../../models/user';
import { Role } from './../../../models/role';
import { getRoles } from '../../catalogues/catalog.selectors';
import * as fromAuth from '../../auth/auth.actions';
import * as fromUser from '../../users/user.actions';
import { AppState } from '../../../reducers';

@Component({
    selector: 'app-user-form',
    templateUrl: './user-form.component.html',
    styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {
    @Input("formData") public formData: { [k: string]: any };
    @Input("user") public user: User;
    @Input("mode") public mode: string;
    public form: FormGroup;
    public submitted: boolean = false;
    public errors: { [k: string]: string[] } = {};
    public avatar: ImageData;
    public roles$: Observable<Role[]>;

    constructor(
        private fb: FormBuilder,
        public translate: TranslateService,
        private store: Store<AppState>
    ) { }

    ngOnInit() {
        let fields = {
            firstName: [this.user ? this.user.firstName : null, Validators.required],
            lastName: [this.user ? this.user.lastName : null, Validators.required],
            email: [this.user ? this.user.email : null, Validators.email],
            password: ['********', this.mode === 'create' ? Validators.required : null],
            passwordConfirmation: ['']
        };
        if (['create', 'edit'].indexOf(this.mode) !== -1) {
            fields['roleId'] = [this.user ? this.user.role.id : null, Validators.required];
        }
        this.form = this.fb.group(fields, {
            validators: [MustMatch('password', 'passwordConfirmation')]
        });
        this.roles$ = this.store.pipe(select(getRoles));
        this.clearForm();
    }

    get roleId() { return this.form.get('roleId'); }
    get firstName() { return this.form.get('firstName'); }
    get lastName() { return this.form.get('lastName'); }
    get email() { return this.form.get('email'); }
    get password() { return this.form.get('password'); }
    get passwordConfirmation() { return this.form.get('passwordConfirmation'); }

    public clearErrors() {
        this.errors = {};
    }

    private clearForm() {
        this.submitted = false;
        this.clearErrors();
    }

    public onAvatarCropped(data: ImageData) {
        this.avatar = { contents: data.contents, name: data.name };
    }

    public submit() {
        this.submitted = true;
        this.clearErrors();
        if (!this.form.valid) {
            return;
        }
        let data = {...this.form.value};
        data.avatarFile = this.avatar != null ? this.avatar : null;
        if (this.mode === 'profile') {
            this.store.dispatch(new fromAuth.UpdateProfile(data));
        }
        else if (this.mode === 'create') {
            this.store.dispatch(new fromUser.AddUser(data));
        }
        else {
            data.id = this.user.id;
            this.store.dispatch(new fromUser.UpdateUser(data));
        }
    }
}
