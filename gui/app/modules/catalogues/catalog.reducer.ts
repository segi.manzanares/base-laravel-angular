import { IRole, Role } from './../../models/role';
import { IApiError } from '../../models/apierror';
import * as fromCatalog from './catalog.actions';

export const cataloguesFeatureKey = 'catalogues';

export interface CatalogState {
    roles: IRole[];
    error: IApiError;
    loading: boolean;
    filterBy: Object;
}

export const initialState: CatalogState = {
    roles: null,
    error: null,
    loading: false,
    filterBy: null,
}

export function reducer(
    state: CatalogState = initialState,
    action: fromCatalog.CatalogActions
): CatalogState {
    switch (action.type) {
        case fromCatalog.CatalogActionTypes.LoadRoles:
            return {...state, loading: true};
        case fromCatalog.CatalogActionTypes.LoadedRoles:
            return {...state, error: null, roles: action.payload, loading: false};
        default:
            return state;
    }
}

export const getRoles = (state: CatalogState) => {
    return state.roles != null ? state.roles.map(c => Role.fromJson(c)) : []
};
export const getError = (state: CatalogState) => state.error;
export const getLoading = (state: CatalogState) => state.loading;
