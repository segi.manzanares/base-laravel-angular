import { Action } from '@ngrx/store';
import { IRole } from '../../models/role';

export enum CatalogActionTypes {
    LoadRoles = '[Catalog] Load Roles',
    LoadedRoles = '[Catalog] Loaded Roles',
}

export class LoadRoles implements Action {
    readonly type = CatalogActionTypes.LoadRoles;

    constructor(public payload: any) { }
}

export class LoadedRoles implements Action {
    readonly type = CatalogActionTypes.LoadedRoles;

    constructor(public payload: IRole[]) { }
}

export type CatalogActions =
    LoadRoles
    | LoadedRoles;
