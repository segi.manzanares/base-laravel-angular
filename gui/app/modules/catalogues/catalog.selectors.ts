import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromCatalog from './catalog.reducer';

export const selectFeature = createFeatureSelector<fromCatalog.CatalogState>(fromCatalog.cataloguesFeatureKey);

export const selectCatalogState = createSelector(
    selectFeature,
    (state: fromCatalog.CatalogState) => state,
);
// Monedas
export const getRoles = createSelector(
    selectCatalogState,
    fromCatalog.getRoles
);

// Errors
export const getError = createSelector(
    selectCatalogState,
    fromCatalog.getError
);
// Loading
export const getLoading = createSelector(
    selectCatalogState,
    fromCatalog.getLoading
);
