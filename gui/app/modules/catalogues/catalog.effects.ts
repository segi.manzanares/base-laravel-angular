import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { CatalogService } from './catalog.service';
import { map, exhaustMap } from 'rxjs/operators';
import {
    CatalogActionTypes,
    LoadRoles,
    LoadedRoles,
} from './catalog.actions';

@Injectable()
export class CatalogEffects {

    @Effect()
    loadCurrencies$ = this.actions$.pipe(
        ofType(CatalogActionTypes.LoadRoles),
        map((action: LoadRoles) => action.payload),
        exhaustMap((payload) =>
            this.catalogService
                .getRoles()
                .pipe(
                    map(resp => new LoadedRoles(resp))
                )
        )
    );

    constructor(
        private actions$: Actions,
        private catalogService: CatalogService,
    ) { }
}
