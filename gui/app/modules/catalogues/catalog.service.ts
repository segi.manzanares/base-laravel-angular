import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ApiService } from './../../services/api.service';
import { IRole } from './../../models/role';

@Injectable({
    providedIn: 'root'
})
export class CatalogService {
    constructor(
        private apiService: ApiService
    ) { }

    public getRoles(): Observable<IRole[]> {
        return this.apiService.request('get', 'catalogues/roles', {});
    }
}
