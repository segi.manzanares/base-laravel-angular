import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgForm, FormBuilder, FormGroup, Validators } from "@angular/forms";
import { select, Store } from "@ngrx/store";
import { AuthService } from "../auth.service";
import { tap, map } from "rxjs/operators";
import { noop, Observable, Subscription } from "rxjs";
import { Router, ActivatedRoute } from "@angular/router";
import * as fromAuth from '../auth.actions';
import { getError, getUserAuthStatus } from '../auth.selectors';
import { AppState } from '../../../reducers';

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    form: FormGroup;
    public loginError: string = '';
    public errors: { [k: string]: string[] } = {};
    public redirectTo: string;
    public submitted: boolean = false;
    public errorSubscription$: Subscription;

    constructor(
        private fb: FormBuilder,
        private auth: AuthService,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private store: Store<AppState>
    ) {
        this.form = fb.group({
            email: ['', [Validators.required]],
            password: ['', [Validators.required]]
        });
        this.clearErrors();
    }

    get email() { return this.form.get('email'); }
    get password() { return this.form.get('password'); }

    ngOnInit() {
        document.getElementsByTagName('body')[0].classList.add('login-page');
        this.store.dispatch(new fromAuth.InitError(null));
        this.activatedRoute.queryParamMap.subscribe(queryParams => {
            this.redirectTo = queryParams.get("redirect")
        });
        this.errorSubscription$ = this.store.pipe(select(getError))
            .subscribe({next: error => {
                console.log(error);
                if (error) {
                    this.loginError = error.message;
                    this.errors = error.errors;
                }
            }});
    }

    ngOnDestroy() {
        document.getElementsByTagName('body')[0].classList.remove('login-page');
        this.errorSubscription$.unsubscribe();
    }

    private clearErrors() {
        this.errors = {};
        Object.keys(this.form.value).forEach(key => this.errors[key] = []);
    }

    login() {
        this.submitted = true;
        this.clearErrors();
        this.loginError = null;
        if (!this.form.valid) {
            return;
        }
        const data = this.form.value;
        this.store.dispatch(new fromAuth.Login({
            email: data.email,
            password: data.password,
            redirect: this.redirectTo
        }));
    }
}
