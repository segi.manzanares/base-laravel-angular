import { Action } from '@ngrx/store';
import { IApiError } from '../../models/apierror';
import { IAuth } from '../../models/auth';
import { IUser, User } from '../../models/user';
import { ApiUpdateResult } from '../../models/apiresponse';

export enum AuthActionTypes {
    InitError = '[Auth] Init Auth Error',
    Login = '[Auth] Login',
    LoginAsUser = '[Auth] LoginAsUser',
    LoginSuccess = '[Auth] Login Success',
    LoginFailed = '[Auth] Login Failed',
    LoginRequired = '[Auth] Login Required',
    Logout = '[Auth] Logout',
    // Recover password
    ForgotPassword = '[Auth] Forgot Password',
    ForgotPasswordSuccess = '[Auth] Forgot Password Success',
    ForgotPasswordFailed = '[Auth] Forgot Password Failed',
    // Reset password
    ResetPassword = '[Auth] Reset Password',
    ResetPasswordSuccess = '[Auth] Reset Password Success',
    ResetPasswordFailed = '[Auth] Reset Password Failed',
    // Update profile
    UpdateProfile = '[Auth] Update Profile',
    UpdateProfileSuccess = '[Auth] Update Profile Success',
    UpdateProfileFailed = '[Auth] Update Profile Failed',
}

export class Login implements Action {
    readonly type = AuthActionTypes.Login;

    constructor(public payload: {email: string, password: string, redirect: string}) { }
}

export class LoginAsUser implements Action {
    readonly type = AuthActionTypes.LoginAsUser;

    constructor(public payload: {userId: number}) { }
}

export class LoginSuccess implements Action {
    readonly type = AuthActionTypes.LoginSuccess;

    constructor(public payload: {auth: IAuth, redirect: string}) { }
}

export class LoginFailed implements Action {
    readonly type = AuthActionTypes.LoginFailed;

    constructor(public payload: IApiError) { }
}

export class LoginRequired implements Action {
    readonly type = AuthActionTypes.LoginRequired;

    constructor(public payload: string) { }
}

export class Logout implements Action {
    readonly type = AuthActionTypes.Logout;
}

export class ForgotPassword implements Action {
    readonly type = AuthActionTypes.ForgotPassword;

    constructor(public payload: {email: string}) { }
}

export class ForgotPasswordSuccess implements Action {
    readonly type = AuthActionTypes.ForgotPasswordSuccess;

    constructor(public payload: string) { }
}

export class ForgotPasswordFailed implements Action {
    readonly type = AuthActionTypes.ForgotPasswordFailed;

    constructor(public payload: IApiError) { }
}

export class ResetPassword implements Action {
    readonly type = AuthActionTypes.ResetPassword;

    constructor(public payload: {email: string, password: string, token: string}) { }
}

export class ResetPasswordSuccess implements Action {
    readonly type = AuthActionTypes.ResetPasswordSuccess;

    constructor(public payload: string) { }
}

export class ResetPasswordFailed implements Action {
    readonly type = AuthActionTypes.ResetPasswordFailed;

    constructor(public payload: IApiError) { }
}

export class UpdateProfile implements Action {
    readonly type = AuthActionTypes.UpdateProfile;

    constructor(public payload: IUser) { }
}

export class UpdateProfileSuccess implements Action {
    readonly type = AuthActionTypes.UpdateProfileSuccess;

    constructor(public payload: ApiUpdateResult<IUser>) { }
}

export class UpdateProfileFailed implements Action {
    readonly type = AuthActionTypes.UpdateProfileFailed;

    constructor(public payload: IApiError) { }
}

export class InitError implements Action {
    readonly type = AuthActionTypes.InitError;

    constructor(public payload: IApiError) { }
}

export type AuthActions =
    | Login
    | LoginAsUser
    | LoginSuccess
    | LoginFailed
    | LoginRequired
    | Logout
    | ForgotPassword
    | ForgotPasswordSuccess
    | ForgotPasswordFailed
    | ResetPassword
    | ResetPasswordSuccess
    | ResetPasswordFailed
    | UpdateProfile
    | UpdateProfileSuccess
    | UpdateProfileFailed
    | InitError