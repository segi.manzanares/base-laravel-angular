import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { StoreModule } from '@ngrx/store';
import { AuthService } from "./auth.service";
import { reducer } from './auth.reducers';
import { AuthGuard } from './auth.guard';
import { IsAuthenticatedGuard } from './is-authenticated.guard';
import { EffectsModule } from '@ngrx/effects';
import { AuthEffects } from './auth.effects';
import { LoginComponent } from './login/login.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { TranslateModule } from '@ngx-translate/core';
import { NgxLoadingModule } from 'ngx-loading';
import { ProfileComponent } from './profile/profile.component';
import { UserModule } from './../users/user.module';

const routes: Routes = [
    { path: 'login', component: LoginComponent, canActivate: [IsAuthenticatedGuard], data: { title: "Login" } },
    { path: 'password/reset', component: ForgotPasswordComponent, canActivate: [IsAuthenticatedGuard], data: { title: "Forgot password" } },
    { path: 'password/reset/:token', component: ResetPasswordComponent, canActivate: [IsAuthenticatedGuard], data: { title: "Reset password" } },
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        UserModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes),
        StoreModule.forFeature('auth', reducer),
        EffectsModule.forFeature([AuthEffects]),
        TranslateModule,
        NgxLoadingModule.forRoot({})
    ],
    declarations: [
        LoginComponent,
        ForgotPasswordComponent,
        ResetPasswordComponent,
        ProfileComponent,
    ],
    exports: [
        LoginComponent,
        ForgotPasswordComponent,
        ResetPasswordComponent,
        ProfileComponent,
        TranslateModule
    ]
})
export class AuthModule {
    static forRoot(): ModuleWithProviders<AuthModule> {
        return {
            ngModule: AuthModule,
            providers: [
                AuthService,
                AuthGuard
            ]
        }
    }
}
