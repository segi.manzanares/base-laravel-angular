import { Component, OnInit, ViewChild, Output, EventEmitter, OnDestroy } from '@angular/core';
import { NgbModal, NgbModalConfig, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { WindowService } from '../../../services/window.service';
import { ToastrService } from 'ngx-toastr';
import { User } from '../../../models/user';
import { defaultNgxLoadingConfig } from '../../../config/index';
import { UserFormComponent } from '../../users/user-form/user-form.component';
import { Store, select, ActionsSubject } from '@ngrx/store';
import { getUser } from '../auth.selectors';
import { Observable, Subscription } from 'rxjs';
import { AppState } from '../../../reducers';
import * as fromAuth from '../auth.actions';
import { getError, getLoading, getSuccessMessage } from '../auth.selectors';
import { filter } from 'rxjs/operators';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy {
    @ViewChild('profileModal', { static: false }) profileModal: NgbModal;
    @Output() profileUpdated: EventEmitter<User> = new EventEmitter();
    public userForm: UserFormComponent;
    public user: User;
    public userSubscription$: Subscription;
    private modalReference: NgbModalRef;
    public loading$: Observable<boolean>;
    public errorSubscription$: Subscription;

    public loader = {
        config: defaultNgxLoadingConfig,
        loading: false
    };

    constructor(
        private store: Store<AppState>,
        config: NgbModalConfig,
        private modalService: NgbModal,
        public windowService: WindowService,
        private toastr: ToastrService,
        private actionsSubject$: ActionsSubject,
    ) {
        config.backdrop = 'static';
        config.keyboard = false;
    }

    ngOnInit() {
        this.store.dispatch(new fromAuth.InitError(null));
        this.userSubscription$ = this.store.pipe(select(getUser))
            .subscribe({ next: user => this.user = user });
        this.errorSubscription$ = this.store.pipe(select(getError)).subscribe({
            next: error => {
                if (error) {
                    this.userForm.errors = error.errors;
                    if (error.statusCode === 401) {
                        this.modalReference.close();
                    }
                }
            }
        });
        this.loading$ = this.store.pipe(select(getLoading));
        this.actionsSubject$.pipe(
            filter((action: any) => action.type === fromAuth.AuthActionTypes.UpdateProfileSuccess)
        ).subscribe((action: any) => {
            this.toastr.success(action.payload.message);
        });
    }

    ngOnDestroy() {
        this.userSubscription$.unsubscribe();
        if (this.errorSubscription$) {
            this.errorSubscription$.unsubscribe();
        }
    }

    public open(content: NgbModal) {
        this.modalReference = this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' });
        this.modalReference.result.then((result) => {
            this.onModalClosed();
        }, (reason) => {
            this.onModalClosed();
        });
    }

    private onModalClosed() {
        this.store.dispatch(new fromAuth.InitError(null));
    }

    public onSubmit(userForm: UserFormComponent): void {
        this.userForm = userForm;
        this.userForm.submit();
    }
}
