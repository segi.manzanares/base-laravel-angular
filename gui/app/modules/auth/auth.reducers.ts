import * as fromAuth from './auth.actions';
import { AuthActionTypes } from './action-types';
import { IAuth, Auth } from '../../models/auth';
import { IApiError } from '../../models/apierror';
import { User, IUser } from '../../models/user';

export interface AuthState {
    isAuthenticated: boolean,
    attemptLoginFailed: boolean,
    credentials: {email: string, password: string},
    profileData: IUser,
    auth: IAuth,
    error: IApiError,
    loading: boolean,
    successMessage: string,
}

export const initialState: AuthState = {
    isAuthenticated: false,
    attemptLoginFailed: false,
    credentials: null,
    profileData: null,
    auth: null,
    error: null,
    loading: false,
    successMessage: null
}

export function reducer(
    state: AuthState = initialState,
    action: fromAuth.AuthActions
): AuthState {
    switch (action.type) {
        case AuthActionTypes.Login:
            return {...state, credentials: {email: action.payload.email, password: action.payload.password}};
        case AuthActionTypes.LoginAsUser:
            return {...state};
        case AuthActionTypes.LoginSuccess:
            return {...state, error: null, auth: action.payload.auth, isAuthenticated: true};
        case AuthActionTypes.LoginFailed:
            return {...state, error: action.payload, attemptLoginFailed: true};
        case AuthActionTypes.Logout:
            return {...initialState};
        case AuthActionTypes.ForgotPassword:
            return {...state, credentials: {...action.payload, password: null}, loading: true};
        case AuthActionTypes.ForgotPasswordSuccess:
            return {...state, error: null, loading: false, successMessage: action.payload};
        case AuthActionTypes.ForgotPasswordFailed:
            return {...state, error: action.payload, loading: false};
        case AuthActionTypes.ResetPassword:
            return {...state, credentials: action.payload, loading: true};
        case AuthActionTypes.ResetPasswordSuccess:
            return {...state, error: null, loading: false, successMessage: action.payload};
        case AuthActionTypes.ResetPasswordFailed:
            return {...state, error: action.payload, loading: false};
        case AuthActionTypes.UpdateProfile:
            return {...state, profileData: action.payload, loading: true};
        case AuthActionTypes.UpdateProfileSuccess:
            return {...state, error: null, loading: false, successMessage: action.payload.message, auth: {...state.auth, user: action.payload.data}};
        case AuthActionTypes.UpdateProfileFailed:
            return {...state, error: action.payload, loading: false};
        case AuthActionTypes.InitError:
            return {...state, error: action.payload, successMessage: null, loading: false};
        default:
            return state;
    }
}

export const isAuthenticated = (state: AuthState) => state.isAuthenticated;
export const getAuth = (state: AuthState) => state.auth ? new Auth(state.auth) : null;
export const getUser = (state: AuthState) => state.auth ? User.fromJson(state.auth.user) : null;
export const getSuperUser = (state: AuthState) => state.auth && state.auth.superUser ? User.fromJson(state.auth.superUser) : null;
export const getError = (state: AuthState) => state.error;
export const getLoading = (state: AuthState) => state.loading;
export const getSuccessMessage = (state: AuthState) => state.successMessage;