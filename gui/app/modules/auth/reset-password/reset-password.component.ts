import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { defaultNgxLoadingConfig } from '../../../config/index';
import { Observable, Subscription } from 'rxjs';
import { Store, select } from '@ngrx/store';
import * as fromAuth from '../auth.actions';
import { getError, getLoading, getSuccessMessage } from '../auth.selectors';
import { AppState } from '../../../reducers';

@Component({
    selector: 'app-reset-password',
    templateUrl: './reset-password.component.html',
    styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
    public formError: string = '';
    public formSuccess: string = '';
    public submitted: boolean = false;
    public form: FormGroup;
    public loading$: Observable<boolean>;
    public successMessage$: Observable<string>;
    public errorSubscription$: Subscription;
    public errors: { [k: string]: string[] } = {};
    public token: string;
    public loader = {
        config: defaultNgxLoadingConfig
    };

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private store: Store<AppState>
    ) {
        this.form = fb.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required]],
        });
        this.clearErrors();
    }

    get email() { return this.form.get('email'); }
    get password() { return this.form.get('password'); }

    private clearErrors() {
        this.submitted = false;
        this.formError = '';
        this.errors = {};
        Object.keys(this.form.value).forEach(key => this.errors[key] = []);
    }

    ngOnInit() {
        this.activatedRoute.paramMap.subscribe(params => {
            this.token = params.get('token');
        });
        this.activatedRoute.queryParamMap.subscribe(queryParams => {
            this.email.setValue(queryParams.get('email'));
        });
        this.store.dispatch(new fromAuth.InitError(null));
        document.getElementsByTagName('body')[0].classList.add('login-page');
        this.errorSubscription$ = this.store.pipe(select(getError)).subscribe({
            next: error => {
                if (error) {
                    this.formError = error.message;
                    this.errors = error.errors;
                }
            }
        });
        this.loading$ = this.store.pipe(select(getLoading));
        this.successMessage$ = this.store.pipe(select(getSuccessMessage));
    }

    ngOnDestroy() {
        document.getElementsByTagName('body')[0].classList.remove('login-page');
        this.errorSubscription$.unsubscribe();
    }

    public onSubmit(): void {
        this.clearErrors();
        this.submitted = true;
        if (!this.form.valid) {
            return;
        }
        this.store.dispatch(new fromAuth.ResetPassword({
            email: this.form.value.email,
            password: this.form.value.password,
            token: this.token,
        }));
    }
}
