import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpErrorResponse } from "@angular/common/http";
import { Observable } from "rxjs";
import { map, catchError, tap } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { User, IUser } from "../../models/user";
import { Auth, IAuth } from "../../models/auth";
import { AppState } from '../../reducers';
import * as AuthActions from './auth.actions';
import { ApiError } from '../../models/apierror';
import { environment } from '../../../environments/environment';
import { Role } from "../../models/role";

@Injectable()
export class AuthService {
    private apiBaseUrl = 'http://localhost:3000/api/v1';

    constructor(
        private http: HttpClient,
        private store: Store<AppState>,
    ) {
        if (environment.apiUrl) {
            this.apiBaseUrl = environment.apiUrl;
        }
    }

    public login(email: string, password: string): Observable<IAuth> {
        let data = {
            username: email,
            password,
            client_id: environment.apiClient.id,
            client_secret: environment.apiClient.secret,
            grant_type: 'password',
        };
        return this.http.post<any>(`${this.apiBaseUrl}/oauth/token`, data)
            /*.pipe(
                map(result => {
                    let user: IUser = {
                        id: result.user.id,
                        firstName: result.user.first_name,
                        lastName: result.user.last_name,
                        email: result.user.email,
                        avatar: result.user.avatar,
                        role: result.user.role
                    };
                    let auth: IAuth = {
                        user: user,
                    };
                    return auth;
                })
            )*/
    }

    public loginAsUser(userId: number): Observable<IAuth> {
        return this.http.post<IAuth>(`${this.apiBaseUrl}/login-as-user`, { userId }, { headers: this.getHeaderData() })
            .pipe(
                tap(auth => localStorage.setItem('auth-super', localStorage.getItem('auth'))),
                catchError((error: any) => {
                    return this.handleError(error);
                })
            );
    }

    public requestToken(): Promise<Auth> {
        let auth = this.getAuthData();
        if (auth && auth.expiresAt > Date.now()) {
            return Promise.resolve(auth);
        }
        const url: string = `${this.apiBaseUrl}/oauth/token`;
        let data = {
            client_id: environment.apiClient.id,
            client_secret: environment.apiClient.secret,
            grant_type: 'client_credentials'
        }
        return this.http
            .post(url, data, {
                headers: {
                    'Accept': 'application/json'
                }
            })
            .toPromise()
            .then(response => {
                let r = new Auth(response);
                //this.saveAuthData(r);
                return r;
            })
            .catch(this.handleError);
    }

    public requestPassword(email: string): Observable<any> {
        return this.http.post<any>(`${this.apiBaseUrl}/password/email`, { email });
    }

    public resetPassword(email: string, password: string, token: string): Observable<any> {
        return this.http.post<any>(`${this.apiBaseUrl}/password/reset`, { email, password, token });
    }

    public updateMe(data: IUser): Observable<{ message: string, data: IUser }> {
        let user = User.fromJson(data);
        user.role = Role.empty();
        user.role.id = data.roleId;
        return this.http.put(`${this.apiBaseUrl}/users/me`, user.toApiRequest(), { headers: this.getHeaderData() })
            .pipe(
                map((resp: any) => {
                    return {
                        message: resp.message,
                        data: resp.data as IUser
                    };
                }),
                catchError((error: any) => {
                    return this.handleError(error);
                })
            );
    }

    private handleError(error) {
        let e = new ApiError(error);
        if (e.statusCode === 401) {
            this.store.dispatch(new AuthActions.LoginRequired('/home'));
        }
        return Promise.reject(e);
    }

    public getAuthenticatedUser(): User {
        const data = this.getAuthData();
        if (data) {
            return User.fromJson(data.user);
        }
        return null;
    }

    public getAuthData(): Auth {
        let data = localStorage.getItem('auth');
        if (data) {
            let jsonAuth = JSON.parse(atob(data));
            if (jsonAuth.auth) {
                return new Auth(jsonAuth.auth);
            }
        }
        return null;
    }

    public isLoggedIn(): boolean {
        const data = this.getAuthData();
        if (data) {
            try {
                return data.expiresAt > Date.now() && data.refreshToken !== null;
            }
            catch (e) {
                return false;
            }

        } else {
            return false;
        }
    }

    public getHeaderData(): HttpHeaders {
        if (!this.isLoggedIn()) {
            this.store.dispatch(new AuthActions.LoginRequired('/home'));
            return null;
        }
        else {
            const data = this.getAuthData();
            let headers = new HttpHeaders({
                'Content-Type': 'application/json',
                Authorization: `${data.tokenType} ${data.accessToken}`
            });
            return headers;
        }
    }
}
