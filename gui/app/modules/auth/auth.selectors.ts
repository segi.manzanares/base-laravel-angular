import {createFeatureSelector, createSelector} from '@ngrx/store';
import * as fromAuth from './auth.reducers';

export const selectFeature = createFeatureSelector<fromAuth.AuthState>('auth');

export const selectAuthState = createSelector(
    selectFeature,
    (state: fromAuth.AuthState) => state,
)

export const getUserAuthStatus = createSelector(
    selectAuthState,
    fromAuth.isAuthenticated
)

export const getAuth = createSelector(
    selectAuthState,
    fromAuth.getAuth
)

export const getUser = createSelector(
    selectAuthState,
    fromAuth.getUser
)

export const getSuperUser = createSelector(
    selectAuthState,
    fromAuth.getSuperUser
)

export const getError = createSelector(
    selectAuthState,
    fromAuth.getError
)

export const getLoading = createSelector(
    selectAuthState,
    fromAuth.getLoading
)

export const getSuccessMessage = createSelector(
    selectAuthState,
    fromAuth.getSuccessMessage
)