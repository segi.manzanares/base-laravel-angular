import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { tap, map, exhaustMap, catchError, switchMap } from 'rxjs/operators';
import { Router, NavigationExtras } from '@angular/router';
import { of } from 'rxjs';
import { AuthService } from './auth.service';
import { ApiError } from '../../models/apierror';

import {
    Login,
    LoginAsUser,
    LoginSuccess,
    LoginFailed,
    LoginRequired,
    ForgotPassword,
    ForgotPasswordSuccess,
    ForgotPasswordFailed,
    ResetPassword,
    ResetPasswordSuccess,
    ResetPasswordFailed,
    UpdateProfile,
    UpdateProfileSuccess,
    UpdateProfileFailed,
    AuthActionTypes,
} from './auth.actions';
import { IUser } from '../../models/user';
import { LoadDashboard } from '../home/home.actions';

@Injectable()
export class AuthEffects {

    @Effect()
    login$ = this.actions$.pipe(
        ofType(AuthActionTypes.Login),
        map((action: Login) => action.payload),
        exhaustMap((payload: any) =>
            this.authService
                .login(payload.email, payload.password)
                .pipe(
                    map(auth => new LoginSuccess({auth, redirect: payload.redirect})),
                    catchError((error: HttpErrorResponse) => {
                        let e = new ApiError(error);
                        return of(new LoginFailed(Object.assign({}, e)));
                    })
                )
        )
    );

    @Effect()
    loginAsUser$ = this.actions$.pipe(
        ofType(AuthActionTypes.LoginAsUser),
        map((action: LoginAsUser) => action.payload),
        exhaustMap((payload: any) =>
            this.authService
                .loginAsUser(payload.userId)
                .pipe(
                    switchMap(auth => [
                        new LoginSuccess({auth, redirect: payload.redirect}),
                        new LoadDashboard({})
                    ]),
                    catchError((error: HttpErrorResponse) => {
                        let e = new ApiError(error);
                        return of(new LoginFailed(Object.assign({}, e)));
                    })
                )
        )
    );

    @Effect({ dispatch: false })
    loginSuccess$ = this.actions$.pipe(
        ofType(AuthActionTypes.LoginSuccess),
        map((action: LoginSuccess) => action.payload),
        tap((payload: any) => {
            let redirect = payload.redirect ?? '/home';
            this.router.navigateByUrl(redirect, {replaceUrl: true});
        })
    );

    @Effect({ dispatch: false })
    loginFailed$ = this.actions$.pipe(
        ofType(AuthActionTypes.LoginFailed)
    );

    @Effect({ dispatch: false })
    loginRedirect$ = this.actions$.pipe(
        ofType(AuthActionTypes.LoginRequired),
        map((action: LoginRequired) => action.payload),
        tap(payload => {
            let navigationExtras: NavigationExtras = {
                queryParams: {'redirect': payload},
                replaceUrl: true
            };
            this.router.navigate(['/auth/login'], navigationExtras);
        })
    );

    @Effect({ dispatch: false })
    logout$ = this.actions$.pipe(
        ofType(AuthActionTypes.Logout),
        tap(() => {
            this.router.navigate(['/auth/login'], {replaceUrl: true});
        })
    );

    // Forgot password

    @Effect()
    forgotPassword$ = this.actions$.pipe(
        ofType(AuthActionTypes.ForgotPassword),
        map((action: ForgotPassword) => action.payload),
        exhaustMap((payload: any) =>
            this.authService
                .requestPassword(payload.email)
                .pipe(
                    map(resp => new ForgotPasswordSuccess(resp.message)),
                    catchError((error: HttpErrorResponse) => {
                        let e = new ApiError(error);
                        return of(new ForgotPasswordFailed(Object.assign({}, e)));
                    })
                )
        )
    );

    @Effect({ dispatch: false })
    forgotPasswordSuccess$ = this.actions$.pipe(
        ofType(AuthActionTypes.ForgotPasswordSuccess)
    );

    @Effect({ dispatch: false })
    forgotPasswordFailed$ = this.actions$.pipe(
        ofType(AuthActionTypes.ForgotPasswordFailed)
    );

    // Reset password

    @Effect()
    resetPassword$ = this.actions$.pipe(
        ofType(AuthActionTypes.ResetPassword),
        map((action: ResetPassword) => action.payload),
        exhaustMap((payload: any) =>
            this.authService
                .resetPassword(payload.email, payload.password, payload.token)
                .pipe(
                    map(resp => new ResetPasswordSuccess(resp.message)),
                    catchError((error: HttpErrorResponse) => {
                        let e = new ApiError(error);
                        return of(new ResetPasswordFailed(Object.assign({}, e)));
                    })
                )
        )
    );

    @Effect({ dispatch: false })
    resetPasswordSuccess$ = this.actions$.pipe(
        ofType(AuthActionTypes.ResetPasswordSuccess)
    );

    @Effect({ dispatch: false })
    resetPasswordFailed$ = this.actions$.pipe(
        ofType(AuthActionTypes.ResetPasswordFailed)
    );

    // Update User Profile

    @Effect()
    updateProfile$ = this.actions$.pipe(
        ofType(AuthActionTypes.UpdateProfile),
        map((action: UpdateProfile) => action.payload),
        exhaustMap((payload: IUser) =>
            this.authService
                .updateMe(payload)
                .pipe(
                    map(resp => new UpdateProfileSuccess(resp)),
                    catchError((error: ApiError) => of(new UpdateProfileFailed(Object.assign({}, error))))
                )
        )
    );

    @Effect({ dispatch: false })
    updateProfileSuccess$ = this.actions$.pipe(
        ofType(AuthActionTypes.UpdateProfileSuccess)
    );

    @Effect({ dispatch: false })
    updateProfileFailed$ = this.actions$.pipe(
        ofType(AuthActionTypes.UpdateProfileFailed)
    );

    constructor(
        private router: Router,
        private actions$: Actions,
        private authService: AuthService,
    ) { }
}