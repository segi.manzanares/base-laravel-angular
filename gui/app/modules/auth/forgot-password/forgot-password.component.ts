import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { defaultNgxLoadingConfig } from '../../../config/index';
import { Router, ActivatedRoute } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { AppState } from '../../../reducers';
import * as fromAuth from '../auth.actions';
import { getError, getLoading, getSuccessMessage } from '../auth.selectors';
import { Observable, Subscription } from 'rxjs';

@Component({
    selector: 'app-forgot-password',
    templateUrl: './forgot-password.component.html',
    styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
    public formError: string = '';
    public formSuccess: string = '';
    public submitted: boolean = false;
    public form: FormGroup;
    public errors: { [k: string]: string[] } = {};
    public loader = {
        config: defaultNgxLoadingConfig
    };
    public loading$: Observable<boolean>;
    public successMessage$: Observable<string>;
    public errorSubscription$: Subscription;

    constructor(
        private fb: FormBuilder,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private store: Store<AppState>
    ) {
        this.form = fb.group({
            email: ['', [Validators.required, Validators.email]]
        });
        this.clearErrors();
    }

    get email() { return this.form.get('email'); }

    private clearErrors() {
        this.submitted = false;
        this.formError = '';
        this.errors = {};
        Object.keys(this.form.value).forEach(key => this.errors[key] = []);
    }

    ngOnInit() {
        this.store.dispatch(new fromAuth.InitError(null));
        document.getElementsByTagName('body')[0].classList.add('login-page');
        this.errorSubscription$ = this.store.pipe(select(getError)).subscribe({next: error => {
            if (error) {
                this.formError = error.message;
                this.errors = error.errors;
            }
        }});
        this.loading$ = this.store.pipe(select(getLoading));
        this.successMessage$ = this.store.pipe(select(getSuccessMessage));
    }

    ngOnDestroy() {
        document.getElementsByTagName('body')[0].classList.remove('login-page');
        this.errorSubscription$.unsubscribe();
    }

    public onSubmit(): void {
        this.clearErrors();
        this.submitted = true;
        if (!this.form.valid) {
            return;
        }
        this.store.dispatch(new fromAuth.ForgotPassword({
            email: this.form.value.email
        }));
    }
}
