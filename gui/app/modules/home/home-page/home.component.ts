import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { AppState } from '../../../reducers';
import { getUserAuthStatus } from '../../auth/auth.selectors';
import { tap } from 'rxjs/operators';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { HomeService } from '../home.service';
import {
    getLoading, getTotalAmount, getTotalPlays, getTopTracks, getTopPlatforms,
    getFilterBy
} from '../home.selectors';
import { defaultNgxLoadingConfig } from '../../../config';
import { LoadDashboard } from '../home.actions';
import Utils from '../../../utils';
import { Moment } from 'moment';
import * as moment from 'moment';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

    constructor(
        private store: Store<AppState>,
        private homeService: HomeService,
    ) {}

    ngOnInit(): void {
    }

    ngOnDestroy(): void {
    }

    public reload() {
    }

    public onChangeMonth(d: Moment) : void {
    }
}
