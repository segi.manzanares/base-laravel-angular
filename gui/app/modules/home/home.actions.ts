import { Action } from '@ngrx/store';

export enum HomeActionTypes {
    LoadDashboard = '[Home] Load Dashboard',
    LoadedDashboard = '[Home] Loaded Dashboard',
    Search = '[Home] Search',
    FinishSearch = '[Home] Finish Search',
    FilterSearchResults = '[Home] Filter Search Results',
}

export class LoadDashboard implements Action {
    readonly type = HomeActionTypes.LoadDashboard;

    constructor(public payload: any) { }
}

export class LoadedDashboard implements Action {
    readonly type = HomeActionTypes.LoadedDashboard;

    constructor(public payload: Object[]) { }
}

export class Search implements Action {
    readonly type = HomeActionTypes.Search;

    constructor(public payload: string) { }
}


export type HomeActions =
    LoadDashboard
    | LoadedDashboard
    | Search;
