import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ApiService } from '../../services/api.service';

@Injectable({
    providedIn: 'root'
})
export class HomeService {
    constructor(
        private apiService: ApiService
    ) { }

    public getDashboardData(filters: Object): Observable<any> {
        return this.apiService.request('get', 'dashboard/data', filters);
    }
}
