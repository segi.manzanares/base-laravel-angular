import * as fromHome from './home.actions';
import { IApiError } from '../../models/apierror';

export const homeFeatureKey = 'home';

export interface HomeState {
    totals: Object;
    totalAmount: Object;
    totalPlays: Object;
    topTracks: Object[];
    topPlatforms: Object;
    searchTerm: string;
    error: IApiError;
    loading: boolean;
    filterBy: Object;
    searchResultsFilterBy: Object;
}

export const initialState: HomeState = {
    totals: null,
    totalAmount: null,
    totalPlays: null,
    topTracks: [],
    topPlatforms: null,
    searchTerm: null,
    error: null,
    loading: false,
    filterBy: null,
    searchResultsFilterBy: null,
}

export function reducer(
    state: HomeState = initialState,
    action: fromHome.HomeActions
): HomeState {
    switch (action.type) {
        case fromHome.HomeActionTypes.LoadDashboard:
            return {...state, loading: true, filterBy: action.payload};
        case fromHome.HomeActionTypes.LoadedDashboard:
            return {
                ...state,
                error: null,
                totals: action.payload['totals'],
                totalAmount: action.payload['totalAmount'],
                totalPlays: action.payload['totalPlays'],
                topTracks: action.payload['topTracks'],
                topPlatforms: action.payload['topPlatforms'],
                filterBy: {...state.filterBy, month: action.payload['month']},
                loading: false
            };
        case fromHome.HomeActionTypes.Search:
            return {...state, loading: true, searchTerm: action.payload, searchResultsFilterBy: null};
        default:
            return state;
    }
}

let getTotal = (state: HomeState, field) => {
    if (state[field] === null) {
        return null;
    }
    return {
        ...state[field],
        chart: {
            ...state[field]['chart'],
            datasets: state[field]['chart']['datasets'].map(r => ({...r}))
        }
    };
}
export const getTotalAmount = (state: HomeState) => {
    return getTotal(state, 'totalAmount');
};
export const getTotalPlays = (state: HomeState) => {
    return getTotal(state, 'totalPlays');
};
export const getTopTracks = (state: HomeState) => state.topTracks;
export const getTopPlatforms = (state: HomeState) => {
    if (state.topPlatforms === null) {
        return null;
    }
    return {
        ...state.topPlatforms,
        datasets: state.topPlatforms['datasets'].map(r => ({...r}))
    };
}
export const getFilterBy = (state: HomeState) => state.filterBy;
export const getError = (state: HomeState) => state.error;
export const getLoading = (state: HomeState) => state.loading;
export const getSearchResultsFilterBy = (state: HomeState) => state.searchResultsFilterBy;
