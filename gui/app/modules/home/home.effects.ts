import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Router } from '@angular/router';
import { HomeService } from './home.service';
import { map, exhaustMap, tap } from 'rxjs/operators';
import {
    HomeActionTypes,
    LoadDashboard,
    LoadedDashboard,
    Search,
} from './home.actions';

@Injectable()
export class HomeEffects {

    @Effect()
    LoadDashboard$ = this.actions$.pipe(
        ofType(HomeActionTypes.LoadDashboard),
        map((action: LoadDashboard) => action.payload),
        exhaustMap((payload) =>
            this.homeService
                .getDashboardData(payload)
                .pipe(
                    map(resp => new LoadedDashboard(resp))
                )
        )
    );

    constructor(
        private router: Router,
        private actions$: Actions,
        private homeService: HomeService,
    ) { }
}
