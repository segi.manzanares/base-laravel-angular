import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromHome from './home.reducer';

export const selectFeature = createFeatureSelector<fromHome.HomeState>(fromHome.homeFeatureKey);

export const selectHomeState = createSelector(
    selectFeature,
    (state: fromHome.HomeState) => state,
);

export const getTotalAmount = createSelector(
    selectHomeState,
    fromHome.getTotalAmount
);

export const getTotalPlays = createSelector(
    selectHomeState,
    fromHome.getTotalPlays
);

export const getTopTracks = createSelector(
    selectHomeState,
    fromHome.getTopTracks
);

export const getTopPlatforms = createSelector(
    selectHomeState,
    fromHome.getTopPlatforms
);

export const getFilterBy = createSelector(
    selectHomeState,
    fromHome.getFilterBy
);

export const getSearchResultsFilterBy = createSelector(
    selectHomeState,
    fromHome.getSearchResultsFilterBy
);

export const getLoading = createSelector(
    selectHomeState,
    fromHome.getLoading
);