import { Component, ViewChild, OnInit, Renderer2, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';import { animate, style, transition, trigger, state } from '@angular/animations';
import { ProfileComponent } from '../../modules/auth/profile/profile.component';
import { AppState } from '../../reducers';
import { AuthService } from '../../modules/auth/auth.service';
import { WindowService } from '../../services/window.service';
import { LoadRoles } from './../../modules/catalogues/catalog.actions';

@Component({
    selector: 'app-main',
    templateUrl: './main.component.html',
    styleUrls: ['./main.component.scss'],
    animations: [
        trigger('architectUIAnimation', [
            state('in', style({ opacity: 1 })),
            transition(':enter', [
                style({ opacity: 0 }),
                animate(600)
            ]),
            transition(':leave', animate(600, style({ opacity: 0 })))
        ])
    ]
})
export class MainComponent implements OnInit, OnDestroy {
    public sidebarMenuOpened = true;
    public sidebarCustomOpened = false;
    @ViewChild('contentWrapper', {static: false}) contentWrapper: any;
    @ViewChild('profile', {static: false}) profile?: ProfileComponent;

    constructor(
        private renderer: Renderer2,
        private authService: AuthService,
        private windowService: WindowService,
        private store: Store<AppState>,
    ) { }

    ngOnInit() {
        this.store.dispatch(new LoadRoles({}));
        if (this.windowService.innerWidth < 992) {
            this.renderer.removeClass(document.body, 'sidebar-open');
            this.renderer.addClass(document.body, 'sidebar-collapse');
            this.sidebarMenuOpened = false;
        }
    }

    ngOnDestroy() {
        // prevent memory leak when component destroyed
        //this.subscription.unsubscribe();
    }

    mainSidebarHeight(height: number) {
        /*this.renderer.setStyle(
            this.contentWrapper.nativeElement,
            'min-height',
            height - 114 + 'px'
        );*/
    }

    toggleMenuSidebar() {
        if (this.sidebarMenuOpened) {
            this.renderer.removeClass(document.body, 'sidebar-open');
            this.renderer.addClass(document.body, 'sidebar-collapse');
            this.sidebarMenuOpened = false;
        }
        else {
            this.renderer.removeClass(document.body, 'sidebar-collapse');
            this.renderer.addClass(document.body, 'sidebar-open');
            this.sidebarMenuOpened = true;
        }
    }

    toggleCustomSidebar() {
        if (this.sidebarCustomOpened) {
            this.renderer.removeClass(document.body, 'control-sidebar-slide-open');
            this.sidebarCustomOpened = false;
        }
        else {
            this.renderer.addClass(document.body, 'control-sidebar-slide-open');
            this.sidebarCustomOpened = true;
        }
    }

    public onProfileUpdated() {
        //this.user = user;
    }

    public openProfile() {
        if (this.profile) {
            this.profile.open(this.profile.profileModal);
        }
    }
}
