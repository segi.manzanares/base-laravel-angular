import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AuthService } from '../../modules/auth/auth.service';
import { Router } from '@angular/router';
import { AppState } from '../../reducers';
import * as fromAuth from '../../modules/auth/auth.actions';
import { getUser } from '../../modules/auth/auth.selectors';
import { User } from '../../models/user';
import { Observable } from 'rxjs';
import { select, Store } from '@ngrx/store';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    @Output() toggleMenuSidebar: EventEmitter<any> = new EventEmitter<any>();
    @Output() openProfile: EventEmitter<any> = new EventEmitter<any>();

    public user$: Observable<User>;

    constructor(
        private store: Store<AppState>,
        private router: Router,
        private authService: AuthService
    ) { }

    ngOnInit() {
        this.user$ = this.store.pipe(select(getUser));
    }

    public doLogout(): void {
        this.store.dispatch(new fromAuth.Logout());
    }
}
