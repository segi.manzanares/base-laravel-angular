import { Component, OnInit } from '@angular/core';
import { animate, style, transition, trigger, state } from '@angular/animations';

@Component({
    selector: 'app-default',
    templateUrl: './default.component.html',
    animations: [
        trigger('architectUIAnimation', [
            state('in', style({ opacity: 1 })),
            transition(':enter', [
                style({ opacity: 0 }),
                animate(600)
            ]),
            transition(':leave', animate(600, style({ opacity: 0 })))
        ])
    ]
})
export class DefaultComponent implements OnInit {

    constructor() {
    }

    ngOnInit() {
    }

}
