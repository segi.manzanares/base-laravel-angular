import { Component, OnInit, AfterViewInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { AppState } from '../../reducers';
import { getUser } from '../../modules/auth/auth.selectors';
import { User } from '../../models/user';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-menu-sidebar',
    templateUrl: './menu-sidebar.component.html',
    styleUrls: ['./menu-sidebar.component.scss']
})
export class MenuSidebarComponent implements OnInit, AfterViewInit {
    @ViewChild('mainSidebar', { static: false }) mainSidebar: any;
    @Output() mainSidebarHeight: EventEmitter<any> = new EventEmitter<any>();
    public activeModule: string;
    public user$: Observable<User>;

    constructor(
        private store: Store<AppState>,
        private router: Router
    ) {
        this.setActiveModule(this.router.url);
    }

    ngOnInit() {
        this.user$ = this.store.pipe(select(getUser));
        this.router.events.subscribe((event) => {
            if (event instanceof NavigationEnd) {
                this.setActiveModule(event.url);
            }
        });
    }

    ngAfterViewInit() {
        this.mainSidebarHeight.emit(this.mainSidebar.nativeElement.offsetHeight);
    }

    public setActiveModule(url: string) {
        if (url.startsWith('/users')) {
            this.activeModule = 'users';
        }
        else if (url.startsWith('/commerces')) {
            this.activeModule = 'commerces';
        }
        else if (url.startsWith('/products')) {
            this.activeModule = 'products';
        }
        else if (url.startsWith('/cashbacks')) {
            this.activeModule = 'cashbacks';
        }
        else if (url.startsWith('/transfers')) {
            this.activeModule = 'transfers';
        }
        else if (url.startsWith('/cashback-offers')) {
            this.activeModule = 'cashback-offers';
        }
        else {
            this.activeModule = 'home';
        }
    }

    public openMenu(event: any) {
        event.preventDefault();
        let parent = event.target.parentElement;
        if (event.target.tagName !== 'A') {
            parent = event.target.closest('a').parentElement;
        }
        if (parent.classList.contains('menu-open')) {
            parent.classList.remove('menu-open');
        }
        else {
            parent.classList.add('menu-open');
        }
    }
}
