import {
    ActionReducer,
    ActionReducerMap,
    MetaReducer
} from '@ngrx/store';
import { environment } from '../../environments/environment';
import { routerReducer, RouterReducerState } from '@ngrx/router-store';
import * as fromAuth from '../modules/auth/auth.reducers';
import * as fromHome from '../modules/home/home.reducer';
import * as fromCatalog from '../modules/catalogues/catalog.reducer';
import { localStorageSync } from 'ngrx-store-localstorage';

const INIT_ACTION = '@ngrx/store/init';

export interface AppState {
    router: RouterReducerState,
    auth: fromAuth.AuthState,
    [fromCatalog.cataloguesFeatureKey]: fromCatalog.CatalogState,
    [fromHome.homeFeatureKey]: fromHome.HomeState,
}

export const reducers: ActionReducerMap<AppState> = {
    router: routerReducer,
    auth: fromAuth.reducer,
    [fromCatalog.cataloguesFeatureKey]: fromCatalog.reducer,
    [fromHome.homeFeatureKey]: fromHome.reducer,
};

export function logger(reducer: ActionReducer<any>): ActionReducer<any> {
    return (state, action) => {
        console.log("state before: ", state);
        console.log("action", action);
        return reducer(state, action);
    }
}

const mergeReducer = (state: any, rehydratedState: any, action: any) => {
    // Inicializar auth state con datos del local storage solo al inicializar la aplicación
    if ((action.type === INIT_ACTION) && rehydratedState) {
        state = { ...state, ...rehydratedState };
    }
    return state;
};

export function localStorageSyncReducer(reducer: ActionReducer<any>): ActionReducer<any> {
    return localStorageSync({
        keys: [
            { auth: ['isAuthenticated', 'attemptLoginFailed', 'credentials', 'auth', 'error'] },
            {
                auth: {
                    encrypt: (state: string) => btoa(state),
                    decrypt: (state: string) => atob(state),
                    deserialize: (raw) => {
                        if (raw.auth && raw.auth.user) {
                            raw.auth.user.createdAt = raw.auth.user.createdAt.toJSON();
                            if (raw.auth.user.updatedAt) {
                                raw.auth.user.updatedAt = raw.auth.user.updatedAt.toJSON();
                            }
                            if (raw.auth.user.role) {
                                raw.auth.user.role.createdAt = raw.auth.user.role.createdAt.toJSON();
                                if (raw.auth.user.role.updatedAt) {
                                    raw.auth.user.role.updatedAt = raw.auth.user.role.updatedAt.toJSON();
                                }
                            }
                        }
                        return raw;
                    }
                }
            }
        ],
        rehydrate: true,
        mergeReducer
    })(reducer);
}

export const metaReducers: MetaReducer<AppState>[] =
    !environment.production ? [logger, localStorageSyncReducer] : [localStorageSyncReducer];
