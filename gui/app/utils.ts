import Swal from 'sweetalert2';

export default class Utils {
    public static confirm(config: {[k: string]: any}) {
        let swalCfg: {[k: string]: any} = {
            title: config.title,
            text: config.msg,
            icon: config.icon === void 0 ? 'warning' : config.icon,
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: config.confirmButtonText ? config.confirmButtonText : "Yes, do it",
            cancelButtonText: config.cancelButtonText ? config.cancelButtonText : "Cancel",
            buttonsStyling: false,
            customClass: {
                confirmButton: 'btn btn-primary',
                cancelButton: 'btn btn-secondary'
            }
        };
        if (config.input !== void 0) {
            swalCfg.input = config.input;
        }
        if (config.inputValue !== void 0) {
            swalCfg.inputValue = config.inputValue;
        }
        if (config.inputPlaceholder !== void 0) {
            swalCfg.inputPlaceholder = config.inputPlaceholder;
        }
        if (config.inputValidator !== void 0) {
            swalCfg.inputValidator = config.inputValidator;
        }
        Swal.fire(swalCfg).then(function(result) {
            if (result.value !== void 0) {
                if (config.input !== void 0) {
                    config.callback(result.value);
                }
                else {
                    config.callback();
                }
            }
        });
    };

    public static alert(config: {[k: string]: any}) {
        let swalCfg: {[k: string]: any} = {
            title: config.title,
            text: config.msg,
            type: config.type === void 0 ? 'info' : config.type,
            confirmButtonColor: '#3085d6',
            confirmButtonText: config.confirmButtonText ? config.confirmButtonText : "OK",
            buttonsStyling: false,
            customClass: {
                confirmButton: 'btn btn-primary'
            }
        };
        Swal.fire(swalCfg).then(function(result) {
            if (result.value !== void 0) {
                if (config.callback) {
                    config.callback();
                }
            }
        });
    };

    public static secondsToHHmmss(seconds: number): string {
        let minutes: number = Math.floor(seconds / 60);
        seconds = seconds % 60;
        let hours: number = Math.floor(minutes / 60);
        minutes = minutes % 60;
        return (hours.toString().length < 2 ? hours.toString().leftPad(2, '0') : hours.toString())
                + ":" + minutes.toString().leftPad(2, '0') + ":" + seconds.toString().leftPad(2, '0');
    }

    public static numberFormat(num: number, decimals: number, decimalPoint?: string, thousandsSeparator?: string): string {
        let n = !isFinite(+num) ? 0 : +num;
        let prec = !isFinite(+decimals) ? 0 : Math.abs(decimals);
        let sep = (typeof thousandsSeparator === 'undefined') ? ',' : thousandsSeparator;
        let dec = (typeof decimalPoint === 'undefined') ? '.' : decimalPoint;
        let toFixedFix = function (n: number, prec: number) {
            var k = Math.pow(10, prec);
            return Math.round(n * k) / k;
        },
        s = (prec ? toFixedFix(n, prec) : Math.round(n)).toString().split('.');
        if (s[0].length > 3) {
            s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
        }
        if ((s[1] || '').length < prec) {
            s[1] = s[1] || '';
            s[1] += new Array(prec - s[1].length + 1).join('0');
        }
        return s.join(dec);
    }
}
