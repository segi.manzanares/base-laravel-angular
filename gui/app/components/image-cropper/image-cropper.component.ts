import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import Cropper from "cropperjs";

export interface ImageData {
    name: string;
    contents: string;
};

@Component({
    selector: 'image-cropper',
    templateUrl: './image-cropper.component.html',
    styleUrls: ['./image-cropper.component.scss']
})
export class ImageCropperComponent implements OnInit {
    @ViewChild('image', { static: false }) image: ElementRef;
    @Input("src") public imageDestination: string;
    @Input("ratio") public aspectRatio?: number;
    @Input("has-error") public hasError?: boolean;
    @Input("preview-class") public previewClass?: string;
    @Input("hide-input") public hideInput?: boolean;
    @Output() imageCropped: EventEmitter<ImageData> = new EventEmitter();

    public imageUrl: string = '';
    private cropper: Cropper;
    public showCropper: boolean = false;
    public hidePreview: boolean = false;
    public data: ImageData = {
        name: null,
        contents: null
    }
    public inputId: string;

    constructor() {
        this.imageDestination = "";
        this.inputId = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(2, 10);
    }

    ngOnInit() {
    }

    public readFile(e: any) {
        if (e.target.files && e.target.files[0]) {
            const reader = new FileReader();
            reader.onload = (e: any) => {
                this.imageUrl = e.target.result;
                this.image.nativeElement.src = this.imageUrl;
                this.initCropper();
            };
            reader.readAsDataURL(e.target.files[0]);
            this.data.name = e.target.files[0].name;
        }
    }

    public initCropper() {
        this.showCropper = true;
        this.cropper = new Cropper(this.image.nativeElement, {
            aspectRatio: this.aspectRatio ? this.aspectRatio : NaN,
            dragMode: 'move',
            movable: true,
            rotatable: true,
            scalable: true,
            zoomable: true,
            checkCrossOrigin: true,
            guides: true,
            viewMode: 1,
            autoCropArea: .5,
            minContainerWidth: 400,
            minContainerHeight: 300,
            background: true
        });
    }

    public crop() {
        this.imageDestination = this.cropper.getCroppedCanvas({
            maxWidth: 720,
            maxHeight: 720
        }).toDataURL();
        this.data.contents = this.imageDestination;
        this.showCropper = false;
        this.imageCropped.emit(this.data);
        this.cropper.destroy();
    }
}
