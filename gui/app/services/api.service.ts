import { catchError } from 'rxjs/operators';
import { Injectable, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { ApiError } from '../models/apierror';
import { AppState } from '../reducers';
import { AuthService } from '../modules/auth/auth.service';
import * as AuthActions from '../modules/auth/auth.actions';
import { environment } from '../../environments/environment';
import { ToastrService } from 'ngx-toastr';

@Injectable({
    providedIn: 'root'
})
export class ApiService {
    private apiBaseUrl = 'http://localhost:3000/api/v1';

    constructor(
        private http: HttpClient,
        private authService: AuthService,
        private router: Router,
        public translate: TranslateService,
        private toastr: ToastrService,
        private store: Store<AppState>,
    ) {
        if (environment.apiUrl) {
            this.apiBaseUrl = environment.apiUrl;
        }
    }


    private handleError(error: any): Promise<any> {
        let e = new ApiError(error);
        if (e.statusCode === 401) {
            this.store.dispatch(new AuthActions.LoginRequired('/home'));
        }
        else if (e.statusCode === 404) {
            this.router.navigate(['404']);
        }
        else if (e.statusCode === 403) {
            this.router.navigate(['403']);
        }
        else if (e.statusCode === 500) {
            this.toastr.error(e.message);
        }
        return Promise.reject(e);
    }

    public downloadExportFile(urlPath: string, type: string, data: { [k: string]: any }): Observable<any> {
        let url: string = `${this.apiBaseUrl}/${urlPath}?export=${type}`;
        const headers = this.authService.getHeaderData();
        const httpOptions: { [k: string]: any } = {
            headers: headers,
            responseType: 'blob'
        };
        if (data) {
            url += '&';
            Object.keys(data).forEach(key => {
                if (data[key] !== null) {
                    url += key + '=' + data[key] + '&';
                }
            });
        }
        return this.http.get(url, httpOptions)
            .pipe(
                catchError(error => this.handleError(error))
            );
    }

    public request(method: string, urlPath: string, data: { [k: string]: any }): Observable<any> {
        let url = `${this.apiBaseUrl}/${urlPath}`;
        let headers = this.authService.getHeaderData();
        let httpOptions = {
            headers: headers
        };
        let req = null;
        if (method === 'get') {
            if (data) {
                url += '?';
                Object.keys(data).forEach(key => {
                    if (data[key] !== null) {
                        url += key + '=' + data[key] + '&';
                    }
                });
            }
            req = this.http.get(url, httpOptions);
        }
        else if (method === 'post' || method === 'put') {
            if (data instanceof FormData) {
                if (httpOptions.headers !== null) {
                    httpOptions.headers = httpOptions.headers.delete('Content-Type');
                }
                httpOptions['reportProgress'] = true;
                httpOptions['observe'] = 'events';
                req = this.http[method](url, data, httpOptions);
            }
            else {
                req = this.http[method](url, data, httpOptions);
            }
        }
        else if (method === 'delete') {
            req = this.http.delete(url, httpOptions);
        }
        return req.pipe(
            catchError((error: any) => {
                return this.handleError(error);
            })
        )
    }
}
