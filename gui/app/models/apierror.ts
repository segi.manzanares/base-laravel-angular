import { HttpErrorResponse } from '@angular/common/http';

export interface IApiError {
    statusCode: number;
    message: string;
    errors: { [k: string]: string[] };
}

export class ApiError implements IApiError {
    statusCode: number;
    message: string;
    errors: { [k: string]: string[] };

    constructor(response: HttpErrorResponse) {
        this.statusCode = response.status;
        this.message = response.error.message ?? null;
        this.errors = response.error.errors ?? {};
    }
}
