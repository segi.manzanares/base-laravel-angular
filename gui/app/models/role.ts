export interface IRole {
    id: number;
    slug: string;
    name: string;
}

export class Role implements IRole {
    constructor(
        public id: number,
        public slug: string,
        public name: string,
    ) { }

    public static fromJson(data: Object): Role {
        return new Role(
            data['id'],
            data['slug'],
            data['name'],
        );
    }

    public static empty(): Role {
        return new Role(0, "", "");
    }
}