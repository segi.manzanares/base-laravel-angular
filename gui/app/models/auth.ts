import { IUser, User } from './user';

export interface IAuth {
    user: IUser;
    superUser?: IUser;
    accessToken: string;
    tokenType: string;
    refreshToken: string;
    expiresIn: number;
};

export class Auth implements IAuth {
    user: User;
    superUser?: User;
    accessToken: string;
    tokenType: string;
    refreshToken: string;
    expiresIn: number;
    expiresAt: number;

    constructor(data: { [k: string]: any }) {
        this.accessToken = data.accessToken;
        this.tokenType = data.tokenType;
        this.refreshToken = data.refreshToken ?? null;
        this.expiresIn = data.expiresIn;
        this.expiresAt = Date.now() + this.expiresIn;
        this.user = data.user ? User.fromJson(data.user) : null;
        this.superUser = data.superUser ? User.fromJson(data.superUser) : null;
    }
}
