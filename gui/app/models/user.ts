import { IRole, Role } from './role';
import { ImageData } from '../components/image-cropper/image-cropper.component';

export interface IUser {
    id: number;
    firstName: string;
    lastName: string;
    email: string;
    isActive: boolean;
    avatar: string;
    avatarFile?: ImageData;
    createdAt: string;
    role: IRole;
    roleId?: number;
}

export class User implements IUser {
    constructor(
        public id: number,
        public firstName: string,
        public lastName: string,
        public email: string,
        public isActive: boolean,
        public avatar: string,
        public createdAt: string,
        public role: Role,
        public avatarFile?: ImageData,
    ) { }

    public static fromJson(data: Object): User {
        return new User(
            data['id'],
            data['firstName'],
            data['lastName'],
            data['email'],
            data['isActive'],
            data['avatar'],
            data['createdAt'],
            data['role'] ? Role.fromJson(data['role']) : null,
            data['avatarFile'],
        );
    }

    get fullName() {
        return this.getFullName();
    }

    public getFullName(): string {
        return this.firstName + ' ' + this.lastName;
    }

    public toApiRequest(): Object {
        let keys = Object.keys(this);
        let json = {};
        keys.forEach(k => json[k.toSnakeCase()] = this[k]);
        json['role_id'] = json['role'] !== null ? json['role']['id'] : null;
        delete json['role'];
        delete json['is_active'];
        return json;
    }
}