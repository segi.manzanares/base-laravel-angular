export interface TableOptions {
    include?: string;
    pagination: {
        page: number,
        take: number
    };
    sort: string;
    filters: Object
}
