export interface ApiListResult<T> {
    data: T[];
    total: number;
}

export interface ApiCreateResult<T> {
    data: T;
    message: string;
}

export interface ApiUpdateResult<T> {
    data: T;
    message: string;
}