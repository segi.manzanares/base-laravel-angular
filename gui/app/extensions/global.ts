export {}

declare global {
    interface String {
        leftPad(padLength: number, padStr: string): string;
        ucFirst(): string;
        toSnakeCase(): string;
    }
}

String.prototype.leftPad = function(padLength: number, padStr: string): string {
    var str = '';
    for (var i = 0; i < padLength; i++) {
        str += padStr;
    }
    str += this;
    return str.slice(-(padStr.length * padLength));
};

String.prototype.ucFirst = function() {
    return this.charAt(0).toUpperCase() + this.substr(1);
};

String.prototype.toSnakeCase = function() {
    return this
        .match(/[A-Z]{2,}(?=[A-Z][a-z]+[0-9]*|\b)|[A-Z]?[a-z]+[0-9]*|[A-Z]|[0-9]+/g)
        .map(x => x.toLowerCase())
        .join('_');
};