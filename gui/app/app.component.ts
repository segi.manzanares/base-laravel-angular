import { filter, map } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { AuthService } from './modules/auth/auth.service';
import { TranslateService } from '@ngx-translate/core';
import { environment } from './../environments/environment';
import { Title } from '@angular/platform-browser';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private titleService: Title,
        public authService: AuthService,
        public translate: TranslateService
    ) {
        translate.addLangs(['en', 'es']);
        translate.setDefaultLang('en');
        let lang = "es";
        translate.use(lang);
    }

    ngOnInit() {
        const title = this.titleService.getTitle();
        this.router
            .events.pipe(
                filter(event => event instanceof NavigationEnd),
                map(() => {
                    let child = this.activatedRoute.firstChild;
                    while (child.firstChild) {
                        child = child.firstChild;
                    }
                    if (child.snapshot.data['title']) {
                        return child.snapshot.data['title'];
                    }
                    return title;
                })
            ).subscribe((t: string) => {
                this.translate.get(t, {}).subscribe((trans) => {
                    this.titleService.setTitle(trans + ' - SySkul');
                });
            });
    }
}
