import { MainComponent } from './layout/main/main.component';
import { DefaultComponent } from './layout/default/default.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './modules/auth/auth.guard';
import { HomeComponent } from './modules/home/home-page/home.component';

const routes: Routes = [
    {
        path: '',
        component: MainComponent,
        canActivate: [AuthGuard],
        canActivateChild: [AuthGuard],
        children: [
            {
                path: '',
                component: HomeComponent,
                data: { title: 'Dashboard' }
            },
            {
                path: 'home',
                component: HomeComponent,
                data: { title: 'Dashboard' }
            },
            {
                path: 'users',
                loadChildren: () => import('./modules/users/user.module').then(m => m.UserModule)
            },
        ]
    },
    {
        path: 'auth',
        component: DefaultComponent,
        loadChildren: () => import('./modules/auth/auth.module').then(m => m.AuthModule)
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
