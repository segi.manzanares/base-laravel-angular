<?php

namespace App\Traits;

use Illuminate\Support\Str;

trait JsonFormat {

    /**
     * Convert the model instance to an array.
     *
     * @return array
     */
    public function toArray()
    {
        $result = array_merge($this->attributesToArray(), $this->relationsToArray());
        return $this->camelCaseKeys($result);
    }

    /**
     * Converts an arrays keys to camel case
     *
     * @param Array $array
     *
     * @return Array
     */
    protected function camelCaseKeys(Array $array)
    {
        $formatted = [];
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $formatted[Str::camel($key)] = $this->camelCaseKeys($value);
            } else {
                $formatted[Str::camel($key)] = $value;
            }
        }
        return $formatted;
    }
}