<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Arr;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $e
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $e)
    {
        if ($e instanceof \Illuminate\Auth\AuthenticationException) {
            if ($e->getMessage() === 'Unauthenticated.' && !empty($e->getTrace()[0]['args'])) {
                if ($e->getTrace()[0]['args'][0] === ['api'] ||
                        (count($e->getTrace()[0]['args']) > 1 && $e->getTrace()[0]['args'][1] === ['api']) ||
                        $e->getTrace()[0]['class'] === 'Laravel\Passport\Http\Middleware\CheckCredentials') {
                    throw new UnauthorizedHttpException(Lang::get("Request denied by the server."), 'unauthorized');
                }
            }
        }
        else if ($e instanceof \League\OAuth2\Server\Exception\OAuthServerException) {
            parent::report($e);
            if ($e->getMessage() === 'The resource owner or authorization server denied the request.') {
                if (!empty($e->getTrace()[0]['args'])) {
                    if ($e->getTrace()[0]['args'][0] === 'Access token could not be verified') {
                        throw new UnauthorizedHttpException(Lang::get("The token is invalid."), 'token_invalid');
                    }
                    else if ($e->getTrace()[0]['args'][0] === 'Access token is invalid') {
                        throw new UnauthorizedHttpException(Lang::get("The token has expired."), 'token_expired');
                    }
                }
            }
            else if ($e->getMessage() === 'The refresh token is invalid.') {
                if (!empty($e->getTrace()[0]['args'])) {
                    if ($e->getTrace()[0]['args'][0] === 'Cannot decrypt the refresh token') {
                        throw new UnauthorizedHttpException(Lang::get("The refresh token is invalid."), 'cannot_decrypt_refresh_token');
                    }
                    else if ($e->getTrace()[0]['args'][0] === 'Token has expired') {
                        throw new UnauthorizedHttpException(Lang::get("The refresh token has expired."), 'refresh_token_expired');
                    }
                    else if ($e->getTrace()[0]['args'][0] === 'Token has been revoked') {
                        throw new UnauthorizedHttpException(Lang::get("The refresh token has been revoked."), 'refresh_token_revoked');
                    }
                }
            }
            else if ($e->getErrorType() === 'invalid_request') {
                throw new BadRequestHttpException($e->getMessage(), null, $e->getErrorType());
            }
            else if ($e->getErrorType() === 'invalid_client') {
                throw new UnauthorizedHttpException($e->getMessage(), $e->getErrorType());
            }
        }
        else {
            parent::report($e);
        }
    }

    /**
     * Convert the given exception to an array.
     *
     * @param  \Throwable  $e
     * @return array
     */
    protected function convertExceptionToArray(Throwable $e)
    {
        $response = config('app.debug') ? [
            'message' => Lang::get($e->getMessage()),
            'exception' => get_class($e),
            'file' => $e->getFile(),
            'line' => $e->getLine(),
            'trace' => collect($e->getTrace())->map(function ($trace) {
                return Arr::except($trace, ['args']);
            })->all(),
        ] : [
            'message' => Lang::get($this->isHttpException($e) ? $e->getMessage() : 'Server error'),
        ];
        $response['code'] = $e->getCode();
        if (method_exists($e, 'getErrors')) {
            $response['errors'] = $e->getErrors();
        }
        return $response;
    }
}
