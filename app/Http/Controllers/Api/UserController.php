<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use App\Traits\FileAction;
use App\Models\User;
use App\Models\Role;
use Carbon\Carbon;

class UserController extends Controller
{
    use FileAction;

    /**
     * The user file types.
     *
     * @var array
     */
    protected $fileTypes = ['avatar'];

    /**
     * Get a listing of users.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->user()->role->slug !== 'admin') {
            abort(403, trans('responses.messages.not_permission'));
        }
        $perPage = $this->perPage;
        $users = User::with(['role', 'avatar']);
        if (!empty($request->input('name'))) {
            $users->where(DB::raw("first_name || ' ' || last_name"), 'ilike', '%' . $request->input('name') . '%');
        }
        if (!empty($request->input('email'))) {
            $users->where('email', 'ilike', '%' . $request->input('email') . '%');
        }
        if (!empty($request->input('role'))) {
            $users->where('role_id', $request->input('role'));
        }
        $sort = explode('|', $request->input('sort'));
        if (count($sort) === 2) {
            if ($sort[0] === 'name') {
                $users->orderBy('first_name', $sort[1])->orderBy('last_name', $sort[1]);
            }
            else {
                $users->orderBy($sort[0], $sort[1]);
            }
        }
        else {
            $users->orderBy('first_name', 'asc')->orderBy('last_name', 'asc');
        }
        if ($request->input('no-paginate') == 1) {
            $users = $users->get();
        }
        else {
            $perPage = $request->input('per_page', $this->perPage);
            $users = $users->paginate($perPage);
        }
        return response()->json($users);
    }

    /**
     * Get the specified user.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id  The user id.
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if ($request->user()->role->slug !== 'admin') {
            abort(403, trans('responses.messages.not_permission'));
        }
        $user = User::with(['avatar', 'role'])->findOrFail($id);
        return response()->json($user);
    }

    /**
     * Get the current user.
     *
     * @return \Illuminate\Http\Response
     */
    public function me(Request $request)
    {
        $user = $request->user();
        $user->avatar;
        $user->role;
        return response()->json($user);
    }

    /**
     * Update the current user.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateMe(UserRequest $request)
    {
        $user = $request->user();
        $data = $request->only(['first_name', 'last_name', 'phone']);
        if ($request->filled('password')) {
            $data['password'] = bcrypt($request->input('password'));
        }
        $user->update($data);
        // Guardar archivos
        $this->storeFiles($request, $user);
        // Enviar respuesta
        $pronoun = trans_choice('responses.pronouns.profile', 1);
        $description = trans('responses.messages.updated', ['model' => $pronoun]);
        // Obtener foto de perfil
        $user->avatar;
        $user->role;
        return response()->json([
            'message' => $description,
            'data' => $user
        ]);
    }

    /**
     * Store a newly created user in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        // Guardar datos del usuario
        $request->merge([
            'password' => bcrypt($request->input('password')),
        ]);
        $data = $request->only([
            'role_id', 'first_name', 'last_name', 'email', 'password', 'phone'
        ]);
        $data['is_active'] = true;
        $user = User::create($data);
        // Guardar archivos
        $this->storeFiles($request, $user);
        $pronoun = trans_choice('responses.pronouns.user', 1);
        $description = trans('responses.messages.created', ['model' => $pronoun]);
        $user->avatar;
        $user->role;
        return response()->json([
            'message' => $description,
            'data' => $user
        ]);
    }

    /**
     * Update the specified user in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id The user id.
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        $user = User::findOrFail($id);
        $data = $request->only([
            'role_id', 'first_name', 'last_name', 'email', 'phone'
        ]);
        if ($request->filled('password')) {
            $data['password'] = bcrypt($request->password);
        }
        if (empty($data['email'])) {
            unset($data['email']);
        }
        $user->update($data);
        // Guardar archivos
        $this->storeFiles($request, $user);
        // Enviar respuesta
        $pronoun = trans_choice('responses.pronouns.user', 1);
        $description = trans('responses.messages.updated', ['model' => $pronoun]);
        // Obtener foto de perfil
        $user->avatar;
        $user->role;
        return response()->json([
            'message' => $description,
            'data' => $user
        ]);
    }

    /**
     * Store user files.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\User $user
     *
     * @return void
     */
    private function storeFiles($request, $user)
    {
        $data = null;
        $oldFile = null;
        $file = null;
        foreach ($this->fileTypes as $type) {
            $file = $request->input("{$type}_file");
            if (!empty($file)) {
                // Si el archivo es de tipo logo_photo, verificamos si existe para reemplazarlo
                $oldFile = $user->files()->where('type', $type)->first();
                $data = [
                    'name' => $file['name'],
                    'storage_path' => $file['path'],
                    'mime' => mime_content_type($file['storage_path']),
                    'size' => filesize($file['storage_path']),
                    'type' => $type
                ];
                // Si ya existe un archivo, reemplazamos
                if ($oldFile !== null) {
                    $this->deleteFile($oldFile, 'public', 'local', false, true, false);
                    $oldFile->update($data);
                } else {
                    $user->files()->create($data);
                }
            }
        }
    }

    /**
     * Remove the specified user from storage.
     *
     * @param  int  $id The user id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->user()->role->slug !== 'admin') {
            abort(403, trans('responses.messages.not_permission'));
        }
        $authUser = $request->user();
        $user = User::findOrFail($id);
        if ($authUser->id == $user->id) {
            return response()->json([
                'message' => Lang::get("Can't delete your own user.")
            ], 422);
        }
        $user->delete();
        // Modificar el email del usuario que se va a eliminar para poder reutilizarlo
        $user->email = $user->email . '|' . Carbon::now()->format('Y.m.d_H.i.s');
        $user->save();
        $pronoun = trans_choice('responses.pronouns.user', 1);
        $description = trans('responses.messages.deleted', ['model' => $pronoun]);
        return response()->json([
            'message' => $description
        ]);
    }

    /**
     * Activate / deactivate the specified user.
     *
     * @param  int  $id The user id.
     * @return \Illuminate\Http\Response
     */
    public function updateStatus($id, Request $request)
    {
        if ($request->user()->role->slug !== 'admin') {
            abort(403, trans('responses.messages.not_permission'));
        }
        $user = User::findOrFail($id);
        // Actualizar estatus
        $user->is_active = !$user->is_active;
        $user->save();
        $pronoun = trans_choice('responses.pronouns.user', 1);
        $description = trans("responses.messages." . ($user->is_active == 1 ? 'activated' : 'deactivated'), ['model' => $pronoun]);
        return response()->json([
            'message' => $description,
            'isActive' => $user->is_active,
        ]);
    }
}
