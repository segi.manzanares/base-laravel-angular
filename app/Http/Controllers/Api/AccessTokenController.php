<?php

namespace App\Http\Controllers\Api;

use Laravel\Passport\TokenRepository;
use Lcobucci\JWT\Parser as JwtParser;
use Psr\Http\Message\ServerRequestInterface;
use Nyholm\Psr7\Response as Psr7Response;
use League\OAuth2\Server\AuthorizationServer;
use Laravel\Passport\Exceptions\OAuthServerException;
use League\OAuth2\Server\Exception\OAuthServerException as LeagueException;
use Illuminate\Http\Response;
use App\Exceptions\UnauthorizedHttpException;
use App\Exceptions\UnprocessableEntityHttpException;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Lang;
use Illuminate\Http\Request;
use Laravel\Passport\Http\Controllers\HandlesOAuthErrors;
use App\Traits\JsonFormat;

class AccessTokenController
{
    use HandlesOAuthErrors, JsonFormat;

    /**
     * The authorization server.
     *
     * @var \League\OAuth2\Server\AuthorizationServer
     */
    protected $server;

    /**
     * The token repository instance.
     *
     * @var \Laravel\Passport\TokenRepository
     */
    protected $tokens;

    /**
     * The JWT parser instance.
     *
     * @var \Lcobucci\JWT\Parser
     */
    protected $jwt;

    /**
     * The authenticated user.
     *
     * @var \App\Models\User
     */
    protected $user;

    /**
     * Create a new controller instance.
     *
     * @param  \League\OAuth2\Server\AuthorizationServer  $server
     * @param  \Laravel\Passport\TokenRepository  $tokens
     * @param  \Lcobucci\JWT\Parser  $jwt
     * @return void
     */
    public function __construct(AuthorizationServer $server, TokenRepository $tokens, JwtParser $jwt)
    {
        $this->jwt = $jwt;
        $this->server = $server;
        $this->tokens = $tokens;
    }

    /**
     * Authorize a client to access the user's account.
     *
     * @param  \Psr\Http\Message\ServerRequestInterface  $request
     * @return \Illuminate\Http\Response
     */
    public function issueToken(ServerRequestInterface $request)
    {
        $params = (array) $request->getParsedBody();
        if ($params['grant_type'] === 'password') {
            $data = [];
            if (array_key_exists('username', $params)) {
                $data['email'] = $params['username'];
            }
            if (array_key_exists('password', $params)) {
                $data['password'] = $params['password'];
            }
            $validator = Validator::make($data, [
                'email' => 'required|email',
                'password' => 'required',
            ]);
            if ($validator->fails()) {
                throw new UnprocessableEntityHttpException(Lang::get("Some fields were incorrect"), $validator->errors(), 'invalid_data');
            }
            // Consultar usuario
            $this->user = User::where('email', strtolower($params['username']))->first();
            if (!empty($this->user)) {
                // Validar contraseña
                if (Hash::check($params['password'], $this->user->password)) {
                    if (!$this->user->is_active) {
                        throw new UnauthorizedHttpException(Lang::get("This user has been temporarily deactivated."), 'temporarily_deactivated');
                    }
                }
            }
        }
        // Solicitar token
        return $this->withErrorHandling(function () use ($request) {
            return $this->convertResponse(
                $this->server->respondToAccessTokenRequest($request, new Psr7Response)
            );
        });
    }

    /**
     * Perform the given callback with exception handling.
     *
     * @param  \Closure  $callback
     * @return mixed
     *
     * @throws \Laravel\Passport\Exceptions\OAuthServerException
     */
    protected function withErrorHandling($callback)
    {
        try {
            return $callback();
        } catch (LeagueException $e) {
            throw new OAuthServerException(
                $e,
                $this->convertResponse($e->generateHttpResponse(new Psr7Response))
            );
        }
    }

    /**
     * Convert a PSR7 response to a Illuminate Response.
     *
     * @param \Psr\Http\Message\ResponseInterface $psrResponse
     * @return \Illuminate\Http\Response
     */
    public function convertResponse($psrResponse)
    {
        $body = json_decode($psrResponse->getBody());
        $statusCode = $psrResponse->getStatusCode();
        if (isset($body->error)) {
            if ($body->error === 'invalid_grant') {
                $body = (object) [
                    'code' => 'invalid_credentials',
                    'message' => Lang::get("Ups! These credentials do not match our records, try again."),
                    'status_code' => $psrResponse->getStatusCode()
                ];
                $statusCode = 401;
            } else if ($body->error === 'invalid_client') {
                $body = (object) [
                    'code' => 'invalid_client',
                    'message' => Lang::get("The client is invalid."),
                    'status_code' => $psrResponse->getStatusCode()
                ];
                $statusCode = 401;
            }
        }
        $body->status_code = $statusCode;
        $body = $this->camelCaseKeys((array) $body);
        if ($this->user !== null) {
            $this->user->role;
            $this->user->avatar;
            $body['user'] = $this->user;
        }
        /*$body = json_encode($body);
        $body = json_decode($body);
        $body->user->createdAt = '2021-01-01';
        unset($body->user->updatedAt);
        unset($body->user->role->createdAt);
        unset($body->user->role->updatedAt);*/
        return new Response(json_encode($body), $statusCode, $psrResponse->getHeaders());
    }

    /**
     * Delete the current user access token.
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteToken(Request $request)
    {
        $this->user = $request->user();
        // Revocar token del usuario
        DB::table('oauth_access_tokens')
            ->where('user_id', $this->user->id)
            ->where('id', $this->user->token()->id)
            ->update(['revoked' => true]);
        // Revocar refresh token
        DB::table('oauth_refresh_tokens')
            ->where('access_token_id', $this->user->token()->id)
            ->update(['revoked' => true]);
        return response()->json(null, 204);
    }
}
