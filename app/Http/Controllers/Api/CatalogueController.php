<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use App\Models\Role;

class CatalogueController extends Controller
{
    /**
     * Listing of available catalogues.
     *
     * @var array
     */
    protected $availableCatalogues = [
        'roles',
    ];

    /**
     * Retrieve a listing of the requested catalogue.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($catalogue, Request $request)
    {
        if ($request->user()->role->slug !== 'admin' && $catalogue !== 'banks') {
            abort(403, trans('responses.messages.not_permission'));
        }
        // Validar catalogo
        if (!in_array($catalogue, $this->availableCatalogues)) {
            abort(404);
        }
        if (method_exists($this, $method = 'get'.Str::studly($catalogue))) {
            return $this->{$method}($request);
        }
        abort(404);
    }

    /**
     * Retrieve a listing of roles.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    private function getRoles(Request $request)
    {
        $roles = Role::orderBy('name')->get();
        return response()->json($roles);
    }
}
