<?php

namespace App\Listeners;

use Laravel\Passport\Events\AccessTokenCreated;
use Laravel\Passport\Token;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class DeleteOldTokens
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \Laravel\Passport\Events\AccessTokenCreated  $event
     * @return void
     */
    public function handle(AccessTokenCreated $event)
    {
        if (!empty($event->userId)) {
            // Obtener tokens expirados del usuario
            $tokens = Token::where('user_id', $event->userId)
                    ->where('id', '!=', $event->tokenId)
                    ->where(function($query) {
                        $query->where('expires_at', '<=', Carbon::now())
                                ->orWhere('revoked', true);
                    })
                    ->get()
                    ->map(function($token) {
                        return $token->id;
                    })
                    ->all();
            if (count($tokens) > 0) {
                // Eliminar access tokens
                DB::table('oauth_access_tokens')
                        ->where('user_id', $event->userId)
                        ->whereIn('id', $tokens)
                        ->delete();
                // Eliminar refresh tokens
                DB::table('oauth_refresh_tokens')
                        ->whereIn('access_token_id', $tokens)
                        ->delete();
            }
        }
        else {
            // Borrar client credentials tokens
            DB::table('oauth_access_tokens')
                    ->whereNull('user_id')
                    ->where(function($query) {
                        $query->where('expires_at', '<=', Carbon::now())
                                ->orWhere('revoked', true);
                    })
                    ->delete();
        }
    }
}