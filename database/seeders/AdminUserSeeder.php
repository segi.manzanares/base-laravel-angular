<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::where('slug', 'admin')->first();
        User::firstOrcreate(["email" => "admin@syskul.com"], [
            'first_name' => 'Admin',
            'last_name' => 'Syskul',
            "password" => bcrypt("secret1234"),
            'role_id' => $role->id,
        ]);
    }
}
